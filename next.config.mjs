/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: [
            'cdn.sanity.io',
            'spacema-dev.com',
            "api.microlink.io", // Microlink Image Preview
        ],
    },
};

export default nextConfig;

// images:{
//     remotePatterns:[
//         {
//             protocol:'https',
//             hostname:'cdn.sanity.io'
//         }
//     ]
// }