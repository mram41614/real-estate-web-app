import Image from 'next/image';
import { urlForImage } from '../lib/image';
import FileViewer from './FileViewer';
import MuxPlayer from '@mux/mux-player-react';
import { LinkPreview } from "@/components/ui/link-preview";


const TableComponent = ({ value }) => {
  if (!value || !value.rows || !Array.isArray(value.rows)) return null;

  return (
    <table className="custom-table">
      <thead>
        <tr>
          {value.rows[0].cells.map((cell, index) => (
            <th key={index} className="custom-th">{cell}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {value.rows.slice(1).map((row, rowIndex) => (
          <tr key={row._key || rowIndex}>
            {row.cells.map((cell, cellIndex) => (
              <td key={cellIndex} className="custom-td">{cell}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};


const FileComponent = ({ value }) => {
  const { fileUrl, title } = value;

  if (!fileUrl) {
    return null; // Return null if there's no fileUrl
  }

  return (
    <div className="my-4">
      <FileViewer url={fileUrl} />
      {/* <a href={fileUrl} target="_blank" rel="noopener noreferrer" className="text-blue-500 underline">
        {title || 'Download File'}
      </a> */}
    </div>
  );
};

// 

const MuxVideoComponent = ({ value }) => {
  if (!value.playbackId) {
    return <p>No video available</p>;
  }

  return (
    <MuxPlayer
      autoPlay
      loop
      muted
      playbackId={value.playbackId}
      metadata={{ video_title: value.title || 'Video' }}
      controls
      style={{ width: '100%', height: 'auto' }}
    />
  );
};


const myPortableTextComponents = {
  types: {
    image: ({ value }) => (
      <div className="my-4">
        <Image
          src={urlForImage(value.imageUrl)}
          alt={value.alt || 'Blog Image'}
          width={800}
          height={600}
          className="rounded"
        />
        {value.imageCaption && (
          <div className="text-center text-sm text-gray-500 mt-2">
            {value.imageCaption}
          </div>
        )}
      </div>
    ),
    file: FileComponent, // Ensure FileComponent is correctly placed under types
    // video: VideoComponent, // Add VideoComponent for rendering video files
    'mux.video': MuxVideoComponent,
    Table: TableComponent,
  },
  marks: {
    link: ({ value, children }) => (
      // <a href={value.href} className="text-blue-500 underline">
      //   {children}
      // </a>
      <LinkPreview 
      url={value.href} 
      className="font-bold bg-clip-text text-transparent bg-gradient-to-br from-purple-500 to-pink-500"
      >
        {children}
      </LinkPreview>
    ),
  },
  block: {
    h1: ({ children }) => (
      <h1 className="text-[1.5rem] font-bold mt-6 mb-2">{children}</h1>
    ),
    h2: ({ children }) => (
      <h2 className="text-[1.1rem] font-bold mt-6 mb-2">{children}</h2>
    ),
    h3: ({ children }) => (
      <h3 className="text-[1.1rem] font-bold mt-6 mb-2">{children}</h3>
    ),
    h4: ({ children }) => (
      <h4 className="text-[1.1rem] font-bold mt-6 mb-2">{children}</h4>
    ),
    p: ({ children }) => (
      <div className='paragraph-container'>
         <p className="my-4">{children}</p>
      </div>
      
    ),
  },
  list: {
    bullet: ({ children }) => (
      <ul className="ml-6 list-disc">{children}</ul>
    ),
    number: ({ children }) => (
      <ol className="ml-6 list-decimal">{children}</ol>
    ),
  },
  listItem: {
    bullet: ({ children }) => (
      <li className="ml-4 list-disc">{children}</li>
    ),
    number: ({ children }) => (
      <li className="ml-4 list-decimal">{children}</li>
    ),
  },
};

export default myPortableTextComponents;


  //   const VideoComponent = ({ value }) => {
  //   const { videoUrl } = value;
  
  //   if (!videoUrl) {
  //     return null; // Return null if there's no videoUrl
  //   }
  
  //   return (
  //     <div className="my-4">
  //       <video controls className="w-full h-auto">
  //         <source src={videoUrl} type="video/mp4" />
  //         Your browser does not support the video tag.
  //       </video>
  //     </div>
  //   );
  // };
