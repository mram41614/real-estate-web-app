import { Inter, Roboto_Mono, Merriweather } from 'next/font/google'
 
export const inter = Inter({
  subsets: ['latin'],
  display: 'swap',
})
 
export const roboto_mono = Roboto_Mono({
  subsets: ['latin'],
  display: 'swap',
})

export const merriweather = Merriweather({
    subsets: ['latin'],
    weight:['300','400','700','900'],
    display: 'swap',
})

