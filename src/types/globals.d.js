export { };

// Create a type for the roles
export const Roles = "admin" | "moderator";
global.CustomJwtSessionClaims =  {
  
    metadata: {
      role : Roles
    }
  
}