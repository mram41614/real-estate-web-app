// 'use client'

// import { useEffect, useState } from "react";
// import Navbar from "../components/Navbar";
// import HomePage from "@/components/HomePage";
// import BlogCard from "@/components/BlogCard";
// import Footer from "@/components/Footer";
// import OurInsightCard from "@/components/OurInsight";
// import Link from "next/link";
// import OurPeopleCard from "@/components/OurPeopleCard";
// import { client } from '../lib/client';
// import Svasa from "@/components/Svasa";
// import { FaArrowRightLong } from "react-icons/fa6";
// import ThreeDCardDemo from "../components/3DCard";
// import { LinkPreview } from "@/components/ui/link-preview";
// import Guidelines from "@/components/Guidelines";


// export default function Home() {
//   const [blogs, setBlogs] = useState([]);
//   const [insights, setInsights] = useState([]);
//   const [person, setPerson] = useState([]);

//   // Fetch initial data
//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const query = `*[_type=='post'] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const insightQuery = `*[_type=='insight'] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const personQuery = `*[_type == 'person' && featured == true] | order(_createdAt asc) {
//           name,
//           "imageUrl": image.asset->url,
//           role,
//           order,
//           description,
//           "slug": slug.current
//         }`;

//         const posts = await client.fetch(query);
//         const insights = await client.fetch(insightQuery);
//         const people = await client.fetch(personQuery);

//         console.log('Fetched Posts:', posts);
//         console.log('Fetched Insights:', insights);
//         console.log('Fetched People:', people);

//         const featuredPosts = posts.filter(post => post.featured);
//         const featuredFromInsights = insights.filter(insight => insight.featured);
//         const sortedPeople = people.sort((a,b)=> a.order - b.order);

//         console.log('Featured Posts:', featuredPosts);
//         console.log('Featured Insights:', featuredFromInsights);

//         setBlogs(featuredPosts);
//         setInsights(featuredFromInsights);
//         setPerson(sortedPeople);
//       } catch (error) {
//         console.error("Error fetching data:", error);
//       }
//     };

//     fetchData();
//   }, []);



//   return (
//     <div>
//       <Navbar />
//       <HomePage />
//       <div>
//       <h1 className="text-[1.5rem] merriweather font-bold text-center mt-20 -mb-5 text-gray-500">The Machani Journal</h1>

//       <div className="flex-grow   mx-auto sm:px-6 lg:px-8 py-12">
//         <section className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4">
//           {blogs.map((blog, index) => (
//             <div key={index} className=" ">
//               <BlogCard blog={blog} url={'how-we-help-clients'} key={index} />
//             </div>
//           ))}
//         </section>
//         <div className="flex justify-center">
//         <Link href="/how-we-help-clients">
//           <button className="bg-transparent merriweather mt-5 hover:bg-blue-800 text-gray-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
//             View All 
//           </button>
//         </Link>
//       </div>
//       </div>
      
//       </div>

//       <div className="mb-8">
//         {/* <Link href='future-of-luxury-retail'> */}
//         <h1 className="text-[1.5rem] merriweather font-bold text-center  mt-5 mb-3 text-gray-500">Regalium - The Future Of Luxury Retail</h1>
//         <section className="mx-6 px-4 text-[1.1rem] h-96">
//       <div className="relative flex flex-col h-full min-w-0 p-4 break-words bg-white shadow-soft-xl rounded-2xl bg-clip-border">
//         <div
//           className="relative h-full overflow-hidden bg-cover rounded-xl"
//           style={{
//             backgroundImage: 'url("/regalium.png")',
//             backgroundSize: 'cover',
//             backgroundPosition: 'center'
//           }}
//         >
//           <div className="absolute inset-0 bg-black opacity-50 rounded-xl"></div>
//           <div className="relative z-10 flex flex-col flex-auto h-full p-4">
//             <p className="text-white text-[1.1rem] work_sans font-light leading-snug">
//               Regalium is an innovative project by the 100-year-old Machani Group in Bangalore - that's deemed to be the future of luxury retail in India. By inculcating futuristic AI that’s nurtured under our portfolio of tech businesses, we’re building a futuristic hub of retail and F&B that envisions altering the experience of retail - from not only the guests’ POV but also the brands.

//               From cutting-edge IoT sensors that cohesively run through the space, VR mirrors and assistants, interactive maps, chat-bots and humanoids at the reception to interactive OOH displays, Regalium is here to change the course of luxury retail and chart out never-seen-before avenues for the industry.
//             </p>
//             <Link href="/future-of-luxury-retail" className="mt-4 flex flex-row mb-0 font-semibold leading-normal text-white group text-sm">
//               <div className="work_sans">Read More</div>
//               <div className="hover:translate-x-2 leading-normal transition-all duration-200">
//                 <FaArrowRightLong className="ease-bounce hover:translate-x-1.25 ml-2 mt-1 leading-normal transition-all duration-200" />
//               </div>
//             </Link>
//           </div>
//         </div>
//       </div>
//     </section>
//           {/* </Link> */}
//       </div>

//        <Guidelines />

//       <div>
//         <h1 className="text-[1.5rem] merriweather font-bold text-center mt-14 mb-3 text-gray-500">Research + Insights</h1>
//         <section className="mx-6 px-4">
//           {insights.map((insight, index) => (
//             <OurInsightCard blog={insight} key={index} />
//           ))}
//         </section>
//       </div>
//       <div className="flex justify-center">
//         <Link href="/our-insights">
//           <button className="bg-transparent merriweather mt-5 hover:bg-blue-800 text-gray-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
//             View All Insights
//           </button>
//         </Link>
//       </div>

//       <div>
//         <h1 className="text-3xl font-bold text-center mt-28 mb-8 text-gray-500">Our People</h1>
//         <section className="m-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
//           {person.map((people, index) => (
//             <OurPeopleCard person={people} key={index} />
//           ))}
//         </section>
//       </div>
//       <div className="flex justify-center mb-12">
//         <Link href="/our-people">
//           <button className="bg-transparent merriweather hover:bg-blue-800 text-gray-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
//             View All People
//           </button>
//         </Link>
//       </div>

//       <Svasa />
//       <Footer />
//     </div>
//   );
// }


// 'use client'

// import Navbar from "@/components/Navbar";
// import { client } from '../../lib/client';
// import { useEffect, useState } from "react";
// import BlogCard from "@/components/BlogCard";
// import Footer from "@/components/Footer";
// // import HeroImage from "@/components/HeroImage"; // Assuming you have a HeroImage component

// export default function Home() {
//   const [blogs, setBlogs] = useState([]);

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const query = `*[_type=='post'] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const posts = await client.fetch(query);
//         setBlogs(posts);
//       } catch (error) {
//         console.error("Error fetching data:", error);
//       }
//     };

//     fetchData();
//   }, []);

//   console.log('Blogs : ', blogs);

//   return (
//     <div className="flex flex-col min-h-screen bg-gray-50   text-gray-900 ">
//       <Navbar />
//       {/* <HeroImage /> */}
//       <div className="flex-grow  mt-20 py-12">
//         <section className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 ">
//           {blogs.map((blog, index) => (
//             <div key={index} className=" hover:text-blue-500 ">
//               <BlogCard blog={blog} url={'how-we-help-clients'} key={index}  />
//             </div>
//           ))}
//         </section>
//       </div>
//       <Footer />
//     </div>
//   );
// }

// Projects Page 

// 'use client'
// import Image from "next/image";
// import Navbar from "@/components/Navbar";
// import { client } from '../../lib/client';
// import { useEffect, useState } from "react";
// import BlogCard from "@/components/BlogCard";
// import Footer from "@/components/Footer";
// import { FaXTwitter } from "react-icons/fa6";
// import { FaFacebookF } from "react-icons/fa";
// import { FaInstagram } from "react-icons/fa";
// import { FaLinkedinIn } from "react-icons/fa6";

// export default function Home() {
//   const [blogs, setBlogs] = useState([]);
//   const [sidePanel,setSidePanel] = useState([]);

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const query = `*[_type=='post'] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
//           sidePanelLinks,
//           socialMediaLinks,
//         }`

//         const [posts,sidePanelData] = await Promise.all([
//           client.fetch(query),
//           client.fetch(sidePanelQuery),
//         ])
        
//         setBlogs(posts);
//         setSidePanel(sidePanelData[0]);
//       } catch (error) {
//         console.error("Error fetching data:", error);
//       }
//     };

//     fetchData();
//   }, []);

//   console.log('Blogs : ', blogs);

//   return (
//     <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
//       <Navbar />
//       <div className="relative w-full h-[500px]">
//         <Image
//           src='/mitbg.jpg'
//           layout="fill"
//           objectFit="cover"
//           alt='mitbg'
//           className="w-full h-full"
//         />
//       </div>
     
//       <div className="flex flex-row">
//       <div className="flex flex-col  mb-5 md:w-[30%] h-[35rem] p-12 z-20 bg-white mt-[-90px] ">
//           <div className="flex flex-col items-center">
//             <div className="flex flex-col text-md items-start gap-5 p-5 ">
//               {sidePanel.sidePanelLinks && sidePanel.sidePanelLinks.map((link, index) => (
//                 <a key={index} href={link.url} className="font-sans text-black hover:text-[#04ADEF] text-center font-bold">
//                   {link.title}
//                 </a>
//               ))}
//               <hr className="my-4 border-t-2 w-full border-gray-300" />
//               <div className="flex flex-row gap-2">
//               {sidePanel?.socialMediaLinks?.map((link, index) => {
//                   let icon;
//                   switch (link.title) {
//                     case 'X':
//                       icon = <FaXTwitter size={18} />;
//                       break;
//                     case 'Facebook':
//                       icon = <FaFacebookF size={18} />;
//                       break;
//                     case 'Instagram':
//                       icon = <FaInstagram size={18} />;
//                       break;
//                     case 'LinkedIn':
//                       icon = <FaLinkedinIn size={18} />;
//                       break;
//                     default:
//                       icon = link.title; // Fallback to the title if no match is found
//                   }
//                   return (
//                     <a key={index} href={link.url} className="text-[#EB008C] hover:text-[#04ADEF]">
//                       {icon}
//                     </a>
//                   );
//                 })}
//               </div>
//             </div>
//           </div>
//         </div>

//         {/* Blog Cards */}
//         <div className="md:w-full bg-white">
//         <section className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3  ">
//           {blogs.map((blog, index) => (
//             <div key={index} className="hover:text-blue-500">
//               <BlogCard blog={blog} url={'how-we-help-clients'} />
//             </div>
//           ))}
//         </section>
//         </div>
//         </div>
//       <Footer />
//     </div>
//   );
// }

// Projects Page 


// 'use client'
// import Image from "next/image";
// import Navbar from "@/components/Navbar";
// import { client } from '../../lib/client';
// import { useEffect, useState } from "react";
// import BlogCard from "@/components/BlogCard";
// import Footer from "@/components/Footer";
// import { FaXTwitter } from "react-icons/fa6";
// import { FaFacebookF } from "react-icons/fa";
// import { FaInstagram } from "react-icons/fa";
// import { FaLinkedinIn } from "react-icons/fa6";

// export default function Home() {
//   const [blogs, setBlogs] = useState([]);
//   const [sidePanel, setSidePanel] = useState(null);
//   const [dropdownOpen, setDropdownOpen] = useState(false);

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const query = `*[_type=='post'] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
//           sidePanelLinks,
//           socialMediaLinks,
//         }`;

//         const [posts, sidePanelData] = await Promise.all([
//           client.fetch(query),
//           client.fetch(sidePanelQuery),
//         ]);

//         setBlogs(posts);
//         setSidePanel(sidePanelData[0]);
//       } catch (error) {
//         console.error("Error fetching data:", error);
//       }
//     };

//     fetchData();
//   }, []);

//   const toggleDropdown = () => {
//     setDropdownOpen(!dropdownOpen);
//   };

//   return (
//     <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
//       <Navbar />
//       <div className="relative w-full h-[500px]">
//         <Image
//           src='/mitbg.jpg'
//           layout="fill"
//           objectFit="cover"
//           alt='mitbg'
//           className="w-full h-full"
//         />
//       </div>
     
//       <div className="flex flex-col md:flex-row">
//         <div className="flex flex-col mb-5 md:w-[30%] p-4 md:p-12 z-20 bg-white mt-[-90px]">
//           <div className="flex flex-col items-center">
//             <div className="flex flex-col text-md items-start min-[320px]:w-full md:w-auto gap-5 p-5">
//               {/* Sidebar links for larger devices */}
//               <div className="hidden md:flex flex-col gap-2 w-full">
//                 {sidePanel?.sidePanelLinks?.map((link, index) => (
//                   <a key={index} href={link.url} className="font-sans text-black hover:text-[#04ADEF]  font-bold">
//                     {link.title}
//                   </a>
//                 ))}
                
//               </div>

//               {/* Sidebar links for smaller devices */}
//               <div className="flex md:hidden w-full flex-col gap-2">
//                 <div className="flex justify-between w-full ">
//                   {sidePanel?.sidePanelLinks?.slice(0, 1).map((link, index) => (
//                     <a key={index} href={link.url} className="font-sans text-black hover:text-[#04ADEF] text-center font-bold">
//                       {link.title}
//                     </a>
//                   ))}
//                   <div className="relative inline-block text-left">
//                     <button
//                       onClick={toggleDropdown}
//                       className="inline-flex justify-center w-auto rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
//                     >
//                       More
//                     </button>
//                     {dropdownOpen && (
//                       <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
//                         <div className="py-1">
//                           {sidePanel?.sidePanelLinks?.slice(1).map((link, index) => (
//                             <a key={index} href={link.url} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
//                               {link.title}
//                             </a>
//                           ))}
//                         </div>
//                       </div>
//                     )}
//                   </div>
//                 </div>
//               </div>

//               <hr className="my-4 border-t-2 w-full border-gray-300" />
//               <div className="flex flex-row  min-[320px]:w-full justify-center md:w-auto gap-4">
//                 {sidePanel?.socialMediaLinks?.map((link, index) => {
//                   let icon;
//                   switch (link.title) {
//                     case 'X':
//                       icon = <FaXTwitter size={18} />;
//                       break;
//                     case 'Facebook':
//                       icon = <FaFacebookF size={18} />;
//                       break;
//                     case 'Instagram':
//                       icon = <FaInstagram size={18} />;
//                       break;
//                     case 'LinkedIn':
//                       icon = <FaLinkedinIn size={18} />;
//                       break;
//                     default:
//                       icon = link.title; // Fallback to the title if no match is found
//                   }
//                   return (
//                     <a key={index} href={link.url} className="text-black hover:text-[#04ADEF]">
//                       {icon}
//                     </a>
//                   );
//                 })}
//               </div>
//             </div>
//           </div>
//         </div>

//         {/* Blog Cards */}
//         <div className="md:w-full bg-white grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3">
//           {blogs.map((blog, index) => (
//             <div key={index} className="hover:text-blue-500">
//               <BlogCard blog={blog} url={'how-we-help-clients'} />
//             </div>
//           ))}
//         </div>
//       </div>
//       <Footer />
//     </div>
//   );
// }