// // pages/projects.js
// 'use client';
// import { useEffect } from 'react';
// import mapboxgl from 'mapbox-gl';
// import 'mapbox-gl/dist/mapbox-gl.css';

// mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_TOKEN;

// export default function Projects() {
//   useEffect(() => {
//     const map = new mapboxgl.Map({
//       container: 'map', // container ID
//       style: 'mapbox://styles/mapbox/light-v10', // style URL
//       center: [-2.81361, 36.77271], // starting position [lng, lat]
//       zoom: 15.5, // starting zoom
//       pitch: 45, // pitch in degrees
//       bearing: -17.6, // bearing in degrees
//       antialias: true, // create the gl context with MSAA antialiasing, so custom layers are antialiased
//     });

//     map.addControl(new mapboxgl.NavigationControl());

//     const layerList = document.getElementById('menu');
//     const inputs = layerList.getElementsByTagName('input');

//     for (const input of inputs) {
//       input.onclick = (layer) => {
//         const layerId = layer.target.id;
//         map.setStyle('mapbox://styles/mapbox/' + layerId);
//       };
//     }

//     map.on('style.load', () => {
//       const layers = map.getStyle().layers;
//       const labelLayerId = layers.find(
//         (layer) => layer.type === 'symbol' && layer.layout['text-field']
//       ).id;

//       map.addLayer(
//         {
//           id: 'add-3d-buildings',
//           source: 'composite',
//           'source-layer': 'building',
//           filter: ['==', 'extrude', 'true'],
//           type: 'fill-extrusion',
//           minzoom: 15,
//           paint: {
//             'fill-extrusion-color': '#aaa',
//             'fill-extrusion-height': [
//               'interpolate',
//               ['linear'],
//               ['zoom'],
//               15,
//               0,
//               15.05,
//               ['get', 'height']
//             ],
//             'fill-extrusion-base': [
//               'interpolate',
//               ['linear'],
//               ['zoom'],
//               15,
//               0,
//               15.05,
//               ['get', 'min_height']
//             ],
//             'fill-extrusion-opacity': 0.6
//           }
//         },
//         labelLayerId
//       );
//     });

//     return () => map.remove(); // Clean up the map instance on component unmount
//   }, []);

//   return (
//     <div>
//       <h1 className="text-4xl font-extrabold text-center my-8">Our Projects</h1>
//       <div id="map" style={{ width: '100%', height: '600px' }}></div>
//       <div id="menu" style={{ position: 'absolute', background: '#efefef', padding: '10px', fontFamily: 'Open Sans, sans-serif' }}>
//         <input id="satellite-streets-v12" type="radio" name="rtoggle" value="satellite" defaultChecked />
//         <label htmlFor="satellite-streets-v12">Satellite Streets</label>
//         <input id="light-v11" type="radio" name="rtoggle" value="light" />
//         <label htmlFor="light-v11">Light</label>
//         <input id="dark-v11" type="radio" name="rtoggle" value="dark" />
//         <label htmlFor="dark-v11">Dark</label>
//         <input id="streets-v12" type="radio" name="rtoggle" value="streets" />
//         <label htmlFor="streets-v12">Streets</label>
//         <input id="outdoors-v12" type="radio" name="rtoggle" value="outdoors" />
//         <label htmlFor="outdoors-v12">Outdoors</label>
//       </div>
//     </div>
//   );
// }
