"use client";

import Image from "next/image";
import React from "react";
import Link from "next/link";
import { CardBody, CardContainer, CardItem } from "./ui/3d-card";
import { PortableText } from "@portabletext/react";
import { urlForImage } from '../lib/image';

const ThreeDCardDemo = ({ blog, url }) => {
  return (
    <CardContainer className="inter-var">
      <CardBody className="bg-gray-50 relative group/card dark:hover:shadow-2xl dark:hover:shadow-emerald-500/[0.1] dark:bg-black dark:border-white/[0.2] border-black/[0.1] w-auto sm:w-[27rem] h-auto rounded-xl p-6 border">
        <Link href={`/${url}/${blog.slug}`}>
          {/* Image Section */}
          <CardItem
            translateZ="100"
            rotateX={20}
            rotateZ={-10}
            className="w-full mb-4"
          >
            <div className="relative h-48 w-full">
              <Image
                src={urlForImage(blog.image)}
                alt="AI for everyone"
                layout="fill"
                objectFit="cover"
                className="rounded-xl"
              />
              <div className="absolute bottom-0 left-4 z-20 w-0 h-0 border-l-[20px] border-l-transparent border-b-[20px] border-b-[#F9FAFB] border-r-[20px] border-r-transparent" />
            </div>
          </CardItem>

          <div className="px-4 h-72 mb-8 hover:text-blue-500">
            {/* Meta Information */}
            <div className="flex justify-between text-neutral-500 text-sm dark:text-neutral-300">
              <CardItem translateZ="60">{blog.tagType}</CardItem>
              <CardItem translateZ="60">
                {blog.publishDate && new Date(blog.publishDate).toLocaleDateString('en-US', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric',
                })}
              </CardItem>
            </div>

            {/* Title and Summary */}
            <div className="flex flex-col justify-between p-4 px-0">
              <CardItem
                translateZ="50"
                className="text-xl garamond text-gray-500 font-semibold line-clamp-2 hover:text-sky-700 hover:underline leading-tight"
              >
                {blog.title} <span className="text-blue-800 font-extrabold text-lg">{'>'}</span>
              </CardItem>
              <CardItem
                as="div"
                translateZ="40"
                className="line-clamp-6 text-gray-400"
              >
                <PortableText value={blog.summary} />
              </CardItem>
            </div>
          </div>
        </Link>
      </CardBody>
    </CardContainer>
  );
}

export default ThreeDCardDemo;
