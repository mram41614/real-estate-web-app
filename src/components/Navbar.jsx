// 'use client'

// import { useState,useEffect } from 'react';
// import axios from 'axios';
// import Image from 'next/image';
// import Link from 'next/link';
// import { FaSearch } from "react-icons/fa";
// import { useRouter } from 'next/navigation';


// function NavBar() {
//   const [navbar, setNavbar] = useState(false);
//   const [chatbotVisible, setChatbotVisible] = useState(false);
//   const [question, setQuestion] = useState('');
//   const [response, setResponse] = useState('');
//   const [searchQuery,setSearchQuery] = useState('');
//   const [searchVisible,setSearchVisible] = useState(false)
//   const router = useRouter();

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     try {
//       const options = {
//         method: 'POST',
//         url: 'https://simple-chatgpt-api.p.rapidapi.com/ask',
//         headers: {
//           'content-type': 'application/json',
//           'X-RapidAPI-Key': process.env.NEXT_PUBLIC_RAPIDAPI_KEY,
//           'X-RapidAPI-Host': 'simple-chatgpt-api.p.rapidapi.com'
//         },
//         data: {
//           question: question
//         }
//       };

//       const response = await axios.request(options);
//       setResponse(response.data);
//     } catch (error) {
//       console.error(error);
//     }
//   };

//   const handleSearchSubmit = (e)=>{
//       e.preventDefault();
//       router.push(`/search?query=${searchQuery}`)
//   }
  
//   const handleWhoWeAreClick = (e) => {
//     e.preventDefault();
//     setNavbar(false); // Close the navbar if open
//     router.push('/#who-we-are'); // Navigate to the home page and scroll to the section
//   };
  
 

//   return (
//     <div>
//       <nav className="w-full bg-transparent fixed top-0 left-0 right-0 z-40">
//         <div className="justify-between px-4 mx-auto md:items-center md:flex ">
//           <div>
//             <div className="flex items-center justify-between py-3 md:py-5 md:block">
//               {/* LOGO */}
//               <Link href="/">
//                 <Image src="/machani-group-logo.png" width={125} height={25} alt="logo" />
//               </Link>
//               {/* HAMBURGER BUTTON FOR MOBILE */}
//               <div className="md:hidden">
//                 <button
//                   className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
//                   onClick={() => setNavbar(!navbar)}
//                 >
//                   {navbar ? (
//                     <Image src="/close.svg" width={30} height={30} alt="close menu" />
//                   ) : (
//                     <Image src="/hamburger-menu.svg" width={30} height={30} alt="open menu" />
//                   )}
//                 </button>
//               </div>
//             </div>
//           </div>
//           <div>
//             <div className={`flex-1 justify-self-center pb-3 mt-8 md:block md:pb-0 md:mt-0 ${navbar ? 'p-12 md:p-0 block' : 'hidden'}`}>
//               <ul  className="h-screen merriweather  text-[1rem]  font-normal  text-gray-950  md:h-auto items-center justify-center md:flex">
//                 {/* <li className="py-2 md:px-6  text-center border-b-2 md:border-b-0 hover:bg-purple-900 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="/how-we-help-clients" onClick={() => setNavbar(!navbar)}>
//                     Projects
//                   </Link>
//                 </li> */}
//                 <li className="py-2 md:px-6  text-center border-b-2 md:border-b-0 hover:bg-purple-900 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="" onClick={handleWhoWeAreClick}>
//                     Who We Are 
//                   </Link>
//                 </li>
//                 <li className="py-2 md:px-6  text-center border-b-2 md:border-b-0 hover:bg-purple-900 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="/how-we-help-clients" onClick={() => setNavbar(!navbar)}>
//                     Projects
//                   </Link>
//                 </li>
//                 {/* <li className="py-2 px-6 text-center border-b-2 md:border-b-0 hover:bg-purple-600 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="/our-insights" onClick={() => setNavbar(!navbar)}>
//                     Our Insights
//                   </Link>
//                 </li> */}
//                 <li className="py-2 md:px-6  text-center border-b-2 md:border-b-0 hover:bg-purple-900 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="/news-updates" onClick={() => setNavbar(!navbar)}>
//                     News + Updates
//                   </Link>
//                 </li>
//                 {/* <li className="py-2 px-6 text-center border-b-2 md:border-b-0 hover:bg-purple-600 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="/our-people" onClick={() => setNavbar(!navbar)}>
//                     Our People
//                   </Link>
//                 </li> */}
//                 <li className="py-2 px-6 text-center border-b-2 md:border-b-0 hover:bg-purple-600 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <button onClick={() => setChatbotVisible(!chatbotVisible)}>
//                     <Image src="/askai.png" width={60} height={60} alt="ask ai"  />
//                   </button>
//                 </li>
//                 <li className="py-2 px-6 text-center border-b-2 md:border-b-0 hover:bg-purple-600 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <form onSubmit={handleSearchSubmit} className="flex items-center">
//                     {
//                       searchVisible===true ? (
//                       <>
//                       <input
//                       type="text"
//                       className="border-gray-400 border text-sm rounded-3xl bg-white p-2"
//                       value={searchQuery}
//                       onChange={(e) => setSearchQuery(e.target.value)}
//                       placeholder="Search..."                  
//                     /> 
//                     <button type="submit" className="-ml-8 text-gray-500">
//                       <FaSearch />
//                     </button>
//                      </>
//                       ):
//                       <button type="submit" onClick={() => setSearchVisible(!searchVisible)} className=" text-gray-500">
//                         <FaSearch />
//                        </button>
//                     }                                  
//                     </form>
                  
//                 </li>
//                 <li className="py-2 px-6 text-center border-b-2 md:border-b-0 hover:bg-purple-600 border-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
//                   <Link href="/contact-us" onClick={() => setNavbar(!navbar)}>
//                     Contact Us
//                   </Link>
//                 </li>
//               </ul>
//             </div>
//           </div>
//         </div>
//       </nav>

//       {chatbotVisible && (
//         <div className="fixed top-24 z-20 right-10 w-80 bg-white border shadow-lg p-4 rounded-lg">
//           <div className="flex justify-between items-center">
//             <h3 className="text-lg font-bold">Ask AI</h3>
//             <button onClick={() => setChatbotVisible(false)}>
//             <Image src="/close.svg" width={30} height={30} alt="close menu" />
//             </button>
//           </div>
//           <form onSubmit={handleSubmit}>
//             <input
//               type="text"
//               className="w-full border text-gray-500 bg-white rounded p-2 mt-2"
//               value={question}
//               onChange={(e) => setQuestion(e.target.value)}
//               placeholder="Ask a question..."
//             />
//             <button type="submit" className="mt-2 bg-blue-500 text-white p-2 rounded">
//               Ask
//             </button>
//           </form>
//           {response && <p className="mt-4">{response.answer}</p>}
//         </div>
//       )}
//     </div>
//   );
// }

// export default NavBar;


'use client'

import { useState, useEffect } from 'react';
import {client} from '../lib/client'
import axios from 'axios';
import Image from 'next/image';
import Link from 'next/link';
import { FaSearch } from "react-icons/fa";
import { useRouter } from 'next/navigation';
import { GiHamburgerMenu } from "react-icons/gi";
import { RxCross2 } from "react-icons/rx";
import { redirect } from "next/navigation";
import { checkRole } from "@/utils/roles";

function NavBar() {
  const [navbar, setNavbar] = useState(false);
  const [chatbotVisible, setChatbotVisible] = useState(false);
  const [question, setQuestion] = useState('');
  const [response, setResponse] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [searchVisible, setSearchVisible] = useState(false);
  const [sidePanel, setSidePanel] = useState(null);
  const [guidelinesOpen, setGuidelinesOpen] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);
  const router = useRouter();

  // if (!checkRole("moderator")) {
  //   redirect("/");
  // }

  const guidelineLinks = [
    { title: "Common", href: "/guide/common" },
    { title: "Retail", href: "/guide/retail" },
    { title: "Office", href: "/guide/office" },
    { title: "F&B", href: "/guide/fnb" },
    { title: "Hotel", href: "/guide/hotel" }
  ];

  useEffect(() => {
    const fetchData = async () => {
      try {
        const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
          sidePanelLinks,
          socialMediaLinks,
        }`;

        const [sidePanelData] = await Promise.all([
          client.fetch(sidePanelQuery),
        ]);

        setSidePanel(sidePanelData[0]);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();

    const handleScroll = () => {
      setScrollPosition(window.scrollY);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const toggleGuidelines = () => {
    setGuidelinesOpen(!guidelinesOpen);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const options = {
        method: 'POST',
        url: 'https://simple-chatgpt-api.p.rapidapi.com/ask',
        headers: {
          'content-type': 'application/json',
          'X-RapidAPI-Key': process.env.NEXT_PUBLIC_RAPIDAPI_KEY,
          'X-RapidAPI-Host': 'simple-chatgpt-api.p.rapidapi.com'
        },
        data: {
          question: question
        }
      };

      const response = await axios.request(options);
      setResponse(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    router.push(`/search?query=${searchQuery}`)
  }

  const handleWhoWeAreClick = (e) => {
    e.preventDefault();
    setNavbar(false); // Close the navbar if open
    router.push('/#who-we-are'); // Navigate to the home page and scroll to the section
  };

  return (
    <div>
      <nav className={`w-full bg-transparent fixed  left-0 right-0 transition-all duration-300 z-40 ${scrollPosition > 100 ?'bg-white top-0':'backdrop-blur-sm bg-slate-50 bg-opacity-5 top-0'}`}>
        <div className="justify-between px-4 mx-auto md:items-center md:flex ">
          <div>
            <div className="flex items-center justify-between py-3 md:py-5 md:block">
              {/* LOGO */}
              <Link href="/">
                <Image src="/machani-group-logo.png" width={125} height={25} alt="logo"  />
              </Link>
              {/* HAMBURGER BUTTON FOR MOBILE */}
              <div className="md:hidden">
                <button
                  className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                  onClick={() => setNavbar(!navbar)}
                >
                  {/* <Image src={navbar ? "/close.svg" : "/hamburger-menu.svg"} width={30} height={30} alt="menu" /> */}
                  {navbar?<RxCross2 />:<GiHamburgerMenu />}
                </button>
              </div>
            </div>
          </div>
          <div className="hidden md:flex items-center space-x-9 mr-14">
            <button onClick={() => setChatbotVisible(!chatbotVisible)}>
              <Image src="/askai.png" width={60} height={60} alt="ask ai" />
            </button>
            <form onSubmit={handleSearchSubmit} className="flex items-center">
              {
                searchVisible ? (
                  <>
                    <input
                      type="text"
                      className="border-gray-400 border text-sm rounded-3xl bg-white p-2"
                      value={searchQuery}
                      onChange={(e) => setSearchQuery(e.target.value)}
                      placeholder="Search..."
                    />
                    <button type="submit" className="-ml-8 text-gray-500">
                      <FaSearch />
                    </button>
                  </>
                ) :
                  <button type="button" onClick={() => setSearchVisible(!searchVisible)} className="text-gray-500">
                    <FaSearch />
                  </button>
              }
            </form>
            <button onClick={() => setNavbar(!navbar)}>
            {navbar?<RxCross2 className='text-3xl text-gray-600 font-bold' />:<GiHamburgerMenu className='text-3xl text-gray-600' />}
            </button>
          </div>
        </div>
      </nav>

      {/* Side Panel */}
      {navbar && (
        <div className="fixed top-0 right-0 md:w-1/2 h-full w-full bg-white z-50 shadow-lg p-4">
          <div className='flex flex-row absolute top-8 right-8'>
          <button className='ml-12' onClick={() => setChatbotVisible(!chatbotVisible)}>
              <Image src="/askai.png" width={60} height={60} alt="ask ai" />
            </button>
            <form onSubmit={handleSearchSubmit} className="flex items-center ml-12">
              {
                searchVisible ? (
                  <>
                    <input
                      type="text"
                      className="border-gray-400 border text-sm rounded-3xl bg-white p-2"
                      value={searchQuery}
                      onChange={(e) => setSearchQuery(e.target.value)}
                      placeholder="Search..."
                    />
                    <button type="submit" className="-ml-8 text-gray-500">
                      <FaSearch />
                    </button>
                  </>
                ) :
                  <button type="button" onClick={() => setSearchVisible(!searchVisible)} className="text-gray-500">
                    <FaSearch />
                  </button>
              }
            </form>
          <button
            className=" ml-12 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
            onClick={() => setNavbar(false)}
          >
            <RxCross2 className='text-3xl text-gray-600 font-bold' />
          </button>
          </div>
          {/* Add your side panel content here */}
          <div className="flex flex-col gap-4 items-start ml-28 mt-40 merriweather text-left text-4xl text-gray-500 ">     
                 {/* {checkRole(req,"moderator")===true?(<Link href='/investor' className=" hover:text-[#04ADEF] font-bold">Investor</Link>):''} */}
                {sidePanel?.sidePanelLinks?.map((link, index) => (
                  <a key={index} href={link.url} className=" hover:text-[#04ADEF] font-bold">
                    {link.title}
                  </a>
                ))}
                <div>
                  <button onClick={toggleGuidelines} className=" hover:text-[#04ADEF] font-bold">
                    Guidelines
                  </button>
                  {guidelinesOpen && (
                    <div className="ml-4 mt-2 flex flex-col gap-2">
                      {guidelineLinks.map((link, index) => (
                        <a key={index} href={`${link.href}`} className=" hover:text-[#04ADEF] font-bold">
                          {link.title}
                        </a>
                      ))}
                    </div>
                  )}
              </div>
              </div>
        </div>
      )}

      {chatbotVisible && (
        <div className="fixed top-24 z-50 right-10 w-80 bg-white border shadow-lg p-4 rounded-lg">
          <div className="flex justify-between items-center">
            <h3 className="text-lg text-gray-500 font-bold">Ask AI</h3>
            <button onClick={() => setChatbotVisible(false)}>
              <RxCross2 className='text-3xl text-gray-600 font-bold' />
            </button>
          </div>
          <form onSubmit={handleSubmit}>
            <input
              type="text"
              className="w-full border text-gray-500 bg-white rounded p-2 mt-2"
              value={question}
              onChange={(e) => setQuestion(e.target.value)}
              placeholder="Ask a question..."
            />
            <button type="submit" className="mt-2 bg-gray-500 text-white p-2 rounded">
              Ask
            </button>
          </form>
          {response && <p className="mt-4">{response.answer}</p>}
        </div>
      )}
    </div>
  );
}

export default NavBar;

