'use client'
import React from 'react'
import BackgroundVideo from 'next-video/background-video'

const Hero = () => {

  const handleScroll = ()=>{
        window.scrollTo({
          top:window.innerHeight,
          behavior:'smooth'
        })
  }

  return (
    <>    
      
        <BackgroundVideo src="/bg-video.mp4"  />
      
      
      <div className='container'>
        <div className='field'>
          <div className='scroll cursor-pointer' onClick={handleScroll}></div>
        </div>
      </div>
    </>
  )
}

export default Hero
