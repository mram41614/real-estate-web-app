// // // "use client";
// // // import React, { useState } from "react";
// // // import Image from "next/image";
// // // import { AnimatePresence, motion } from "framer-motion";
// // // import { BackgroundBeams } from "@/components/ui/background-beams";
// // // import { BackgroundGradient } from "@/components/ui/background-gradient";
// // // import Link from "next/link";
// // // import { FaLink } from "react-icons/fa";

// // // const cardVariants = {
// // //   hidden: { opacity: 0, y: 20 },
// // //   visible: { opacity: 1, y: 0 },
// // // };

// // // const containerVariants = {
// // //   hidden: { opacity: 1 },
// // //   visible: {
// // //     opacity: 1,
// // //     transition: {
// // //       staggerChildren: 0.1,
// // //     },
// // //   },
// // // };

// // // const Card = React.memo(({ text, image, guidelines, onClick, isActive }) => {
// // //   const [hovered, setHovered] = React.useState(false);

// // //   const handleMouseMove = (e) => {
// // //     const card = e.currentTarget;
// // //     const rect = card.getBoundingClientRect();
// // //     const x = e.clientX - rect.left;
// // //     const y = e.clientY - rect.top;
// // //     const centerX = rect.width / 2;
// // //     const centerY = rect.height / 2;
// // //     const rotateX = -(y - centerY) / 10;
// // //     const rotateY = (x - centerX) / 10;

// // //     card.style.transform = `perspective(1000px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
// // //   };

// // //   const handleMouseLeave = (e) => {
// // //     const card = e.currentTarget;
// // //     card.style.transform = "perspective(1000px) rotateX(0deg) rotateY(0deg)";
// // //   };

// // //   return (
// // //     <motion.div
// // //       className={`h-96 rounded-lg flex flex-col items-center justify-center w-full gap-4 mx-auto px-8 relative overflow-hidden transition-transform duration-300 ${isActive ? 'bg-white' : ''}`}
// // //       onMouseEnter={() => setHovered(true)}
// // //       onMouseLeave={(e) => {
// // //         setHovered(false);
// // //         handleMouseLeave(e);
// // //       }}
// // //       onMouseMove={handleMouseMove}
// // //       onClick={onClick}
// // //       variants={cardVariants}
// // //     >
// // //       <div className="absolute inset-0 z-0">
// // //       </div>
// // //       <p className={`md:text-2xl text-2xl font-medium text-center ${isActive ? 'text-black' : 'text-white'} relative z-20 max-w-2xl mx-auto`}>
// // //         {text}
// // //       </p>
// // //       <AnimatePresence>
// // //         {hovered && !isActive && (
// // //           <motion.div
// // //             initial={{ opacity: 0 }}
// // //             animate={{ opacity: 1 }}
// // //             exit={{ opacity: 0 }}
// // //             className="h-full w-full absolute inset-0 z-10"
// // //           >
// // //             <Image
// // //               src={image}
// // //               layout="fill"
// // //               objectFit="cover"
// // //               className="absolute inset-0 w-full h-full object-cover"
// // //               alt="Hover Image"
// // //             />
// // //           </motion.div>
// // //         )}
// // //       </AnimatePresence>
// // //       {isActive && (
// // //         <div className="p-4 -mt-10 z-30">
// // //           <h2 className="text-xl font-bold mb-4">Explore These Guidelines</h2>
// // //           <ul>
// // //             {guidelines.map((guideline, index) => (
// // //               <li key={index} className="text-black text-lg mb-2 flex items-center">
// // //                 <FaLink className="mr-2 text-blue-700" />
// // //                 <Link href={`/guidelines/common/${guideline.slug}`} className="text-blue-700 hover:underline hover:text-blue-900">
// // //                   {guideline.title}
// // //                 </Link>
// // //               </li>
// // //             ))}
// // //           </ul>
// // //         </div>
// // //       )}
// // //       <div className={`absolute inset-0 [mask-image:radial-gradient(400px_at_center,white,transparent)] ${isActive ? 'bg-white' : 'bg-black/50 dark:bg-black/90'} z-10`} />
// // //     </motion.div>
// // //   );
// // // });

// // // const LuxuryCard = ({ data }) => {
// // //   const [isActive, setIsActive] = useState(false);

// // //   const handleCardClick = () => {
// // //     setIsActive(!isActive);
// // //   };

// // //   return (
// // //     <>
// // //       <div className="mx-auto rounded-md overflow-auto">
// // //         <motion.div
// // //           className="relative z-10 w-full flex items-center justify-center"
// // //           variants={containerVariants}
// // //           initial="hidden"
// // //           animate="visible"
// // //         >
// // //           <BackgroundGradient>
// // //             <Card
// // //               key={data.id}
// // //               text={data.title}
// // //               image={data.image}
// // //               guidelines={data.guidelinesCommon || []}
// // //               onClick={handleCardClick}
// // //               isActive={isActive}
// // //             />
// // //           </BackgroundGradient>
// // //         </motion.div>
// // //       </div>
// // //       <BackgroundBeams />
// // //     </>
// // //   );
// // // };

// // // export default LuxuryCard;

// // "use client";
// // import React, { useState } from "react";
// // import Image from "next/image";
// // import { AnimatePresence, motion } from "framer-motion";
// // import { BackgroundBeams } from "@/components/ui/background-beams";
// // import { BackgroundGradient } from "@/components/ui/background-gradient";
// // import Link from "next/link";
// // import { FaLink } from "react-icons/fa";

// // const cardVariants = {
// //   hidden: { opacity: 0, y: 20 },
// //   visible: { opacity: 1, y: 0 },
// // };

// // const containerVariants = {
// //   hidden: { opacity: 1 },
// //   visible: {
// //     opacity: 1,
// //     transition: {
// //       staggerChildren: 0.1,
// //     },
// //   },
// // };

// // const Card = React.memo(({ text, image, guidelines, isActive, onClick }) => {
// //   const [hovered, setHovered] = useState(false);

// //   const handleMouseMove = (e) => {
// //     const card = e.currentTarget;
// //     const rect = card.getBoundingClientRect();
// //     const x = e.clientX - rect.left;
// //     const y = e.clientY - rect.top;
// //     const centerX = rect.width / 2;
// //     const centerY = rect.height / 2;
// //     const rotateX = -(y - centerY) / 10;
// //     const rotateY = (x - centerX) / 10;

// //     card.style.transform = `perspective(1000px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
// //   };

// //   const handleMouseLeave = (e) => {
// //     const card = e.currentTarget;
// //     card.style.transform = "perspective(1000px) rotateX(0deg) rotateY(0deg)";
// //   };

// //   return (
// //     <motion.div
// //       className={`h-96 rounded-lg flex flex-col items-center justify-center w-full gap-4 mx-auto px-8 relative overflow-hidden transition-transform duration-300 ${isActive ? 'bg-white' : ''}`}
// //       onMouseEnter={() => setHovered(true)}
// //       onMouseLeave={(e) => {
// //         setHovered(false);
// //         handleMouseLeave(e);
// //       }}
// //       onMouseMove={handleMouseMove}
// //       onClick={onClick}
// //       variants={cardVariants}
// //     >
// //       <div className="absolute inset-0 z-0">
// //       </div>
// //       <p className={`md:text-2xl text-2xl font-medium text-center ${isActive ? 'text-black' : 'text-white'} relative z-20 max-w-2xl mx-auto`}>
// //         {text}
// //       </p>
// //       <AnimatePresence>
// //         {hovered && !isActive && (
// //           <motion.div
// //             initial={{ opacity: 0 }}
// //             animate={{ opacity: 1 }}
// //             exit={{ opacity: 0 }}
// //             className="h-full w-full absolute inset-0 z-10"
// //           >
// //             <Image
// //               src={image}
// //               layout="fill"
// //               objectFit="cover"
// //               className="absolute inset-0 w-full h-full object-cover"
// //               alt="Hover Image"
// //             />
// //           </motion.div>
// //         )}
// //       </AnimatePresence>
// //       {isActive && guidelines && (
// //         <div className="p-4 -mt-10 z-30">
// //           <h2 className="text-xl font-bold mb-4">Explore These Guidelines</h2>
// //           <ul>
// //             {guidelines.map((guideline, index) => (
// //               <li key={index} className="text-black text-lg mb-2 flex items-center">
// //                 <FaLink className="mr-2 text-blue-700" />
// //                 <Link href={`/guidelines/${slug}/${guideline.slug}`} className="text-blue-700 hover:underline hover:text-blue-900">
// //                   {guideline.title}
// //                 </Link>
// //               </li>
// //             ))}
// //           </ul>
// //         </div>
// //       )}
// //       <div className={`absolute inset-0 [mask-image:radial-gradient(400px_at_center,white,transparent)] ${isActive ? 'bg-white' : 'bg-black/50 dark:bg-black/90'} z-10`} />
// //     </motion.div>
// //   );
// // });

// // const LuxuryCard = ({ data, guidelines, isActive,slug }) => {
// //   return (
// //     <>
// //       <div className="mx-auto rounded-md overflow-auto">
// //         <motion.div
// //           className="relative z-10 w-full flex items-center justify-center"
// //           variants={containerVariants}
// //           initial="hidden"
// //           animate="visible"
// //         >
// //           <BackgroundGradient>
// //             <Card
// //               key={data.id}
// //               text={data.title}
// //               image={data.image}
// //               slug = {slug}
// //               guidelines={guidelines}
// //               isActive={isActive}
// //             />
// //           </BackgroundGradient>
// //         </motion.div>
// //       </div>
// //       <BackgroundBeams />
// //     </>
// //   );
// // };

// // export default LuxuryCard;

// "use client";
// import React, { useState } from "react";
// import Image from "next/image";
// import { AnimatePresence, motion } from "framer-motion";
// import { BackgroundBeams } from "@/components/ui/background-beams";
// import { BackgroundGradient } from "@/components/ui/background-gradient";
// import Link from "next/link";
// import { FaLink } from "react-icons/fa";

// const cardVariants = {
//   hidden: { opacity: 0, y: 20 },
//   visible: { opacity: 1, y: 0 },
// };

// const containerVariants = {
//   hidden: { opacity: 1 },
//   visible: {
//     opacity: 1,
//     transition: {
//       staggerChildren: 0.1,
//     },
//   },
// };

// const Card = React.memo(({ text, image, guidelines, isActive, onClick, slug }) => {
//   const [hovered, setHovered] = useState(false);

//   const handleMouseMove = (e) => {
//     const card = e.currentTarget;
//     const rect = card.getBoundingClientRect();
//     const x = e.clientX - rect.left;
//     const y = e.clientY - rect.top;
//     const centerX = rect.width / 2;
//     const centerY = rect.height / 2;
//     const rotateX = -(y - centerY) / 10;
//     const rotateY = (x - centerX) / 10;

//     card.style.transform = `perspective(1000px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
//   };

//   const handleMouseLeave = (e) => {
//     const card = e.currentTarget;
//     card.style.transform = "perspective(1000px) rotateX(0deg) rotateY(0deg)";
//   };

//   return (
//     <motion.div
//       className={`h-96 w-80 rounded-lg flex flex-col items-center justify-center gap-4 mx-auto px-8 relative overflow-hidden transition-transform duration-300 ${isActive ? 'bg-white' : ''}`}
//       onMouseEnter={() => setHovered(true)}
//       onMouseLeave={(e) => {
//         setHovered(false);
//         handleMouseLeave(e);
//       }}
//       onMouseMove={handleMouseMove}
//       onClick={onClick}
//       variants={cardVariants}
//     >
//       <div className="absolute inset-0 z-0">
//       </div>
//       <p className={`md:text-2xl text-2xl font-medium text-center ${isActive ? 'text-black' : 'text-white'} relative z-20 max-w-2xl mx-auto`}>
//         {text}
//       </p>
//       <AnimatePresence>
//         {hovered && !isActive && (
//           <motion.div
//             initial={{ opacity: 0 }}
//             animate={{ opacity: 1 }}
//             exit={{ opacity: 0 }}
//             className="h-full w-full absolute inset-0 z-10"
//           >
//             <Image
//               src={image}
//               layout="fill"
//               objectFit="cover"
//               className="absolute inset-0 w-full h-full object-cover"
//               alt="Hover Image"
//             />
//           </motion.div>
//         )}
//       </AnimatePresence>
//       {isActive && guidelines && (
//         <div className="p-4 -mt-10 z-30">
//           <h2 className="text-xl font-bold mb-4">Explore These Guidelines</h2>
//           <ul>
//             {guidelines && guidelines.map((guideline, index) => (
//               <li key={index} className="text-black text-lg mb-2 flex items-center">
//                 <FaLink className="mr-2 text-blue-700" />
//                 <Link href={`/guidelines/${slug}/${guideline.slug}`} className="text-blue-700 hover:underline hover:text-blue-900">
//                   {guideline.title}
//                 </Link>
//               </li>
//             ))}
//           </ul>
//         </div>
//       )}
//       <div className={`absolute inset-0 [mask-image:radial-gradient(400px_at_center,white,transparent)] ${isActive ? 'bg-white' : 'bg-black/50 dark:bg-black/90'} z-10`} />
//     </motion.div>
//   );
// });

// const LuxuryCard = ({ data, guidelines, isActive, slug }) => {
//   return (
//     <>
//       <div className="mx-auto rounded-md overflow-auto">
//         <motion.div
//           className="relative z-10 w-full flex items-center justify-center"
//           variants={containerVariants}
//           initial="hidden"
//           animate="visible"
//         >
//           <BackgroundGradient>
//             <Card
//               key={data.id}
//               text={data.title}
//               image={data.image}
//               guidelines={guidelines}
//               isActive={isActive}
//               slug={slug}
//             />
//           </BackgroundGradient>
//         </motion.div>
//       </div>
//       <BackgroundBeams />
//     </>
//   );
// };

// export default LuxuryCard;


"use client";
import React, { useState } from "react";
import Image from "next/image";
import { AnimatePresence, motion } from "framer-motion";
import { BackgroundBeams } from "@/components/ui/background-beams";
import { BackgroundGradient } from "@/components/ui/background-gradient";
import Link from "next/link";
import { FaLink } from "react-icons/fa";

const cardVariants = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const containerVariants = {
  hidden: { opacity: 1 },
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const Card = React.memo(({ text, image, guidelines, isActive, onClick, slug }) => {
  const [hovered, setHovered] = useState(false);

  const handleMouseMove = (e) => {
    const card = e.currentTarget;
    const rect = card.getBoundingClientRect();
    const x = e.clientX - rect.left;
    const y = e.clientY - rect.top;
    const centerX = rect.width / 2;
    const centerY = rect.height / 2;
    const rotateX = -(y - centerY) / 10;
    const rotateY = (x - centerX) / 10;

    card.style.transform = `perspective(1000px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
  };

  const handleMouseLeave = (e) => {
    const card = e.currentTarget;
    card.style.transform = "perspective(1000px) rotateX(0deg) rotateY(0deg)";
  };

  return (
    <motion.div
      className={`h-96 w-80 rounded-3xl flex flex-col items-center justify-center gap-4 mx-auto px-8 relative overflow-hidden transition-transform duration-300 ${isActive ? 'bg-[#EDD4BE]' : ''}`}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={(e) => {
        setHovered(false);
        handleMouseLeave(e);
      }}
      onMouseMove={handleMouseMove}
      onClick={onClick}
      variants={cardVariants}
    >
      <div className="absolute inset-0 z-0">
      </div>
      <p className={`md:text-2xl text-2xl font-medium text-center ${isActive ? 'text-black' : 'text-white'} relative z-20 max-w-2xl mx-auto`}>
        {text}
      </p>
      <AnimatePresence>
        {hovered && !isActive && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className="h-full w-full  absolute inset-0 z-10"
          >
            <Image
              src={image}
              layout="fill"
              objectFit="cover"
              className="absolute inset-0 rounded-3xl w-full h-full object-cover"
              alt="Hover Image"
            />
          </motion.div>
        )}
      </AnimatePresence>
      {isActive && guidelines && (
        <div className="p-4 -mt-8 z-30  overflow-y-auto max-h-60">
          <h2 className="text-xl  font-bold mb-4">Explore These Guidelines</h2>
          <ul>
            {guidelines.map((guideline, index) => (
              <li key={index} className="text-black text-lg mb-2 flex items-center">
                <FaLink className="mr-2 text-blue-700" />
                <Link href={`/guidelines/${slug}/${guideline.slug}`} className="text-blue-700 hover:underline hover:text-blue-900">
                  {guideline.title}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className={`absolute inset-0 [mask-image:radial-gradient(400px_at_center,white,transparent)] ${isActive ? 'bg-transparent' : 'bg-black/50 dark:bg-black/90'} z-10`} />
    </motion.div>
  );
});

const LuxuryCard = ({ data, guidelines, isActive, slug }) => {
  const [cardActive, setCardActive] = useState(isActive);

  const handleCardClick = () => {
    setCardActive(!cardActive);
  };

  return (
    <>
      <div className="mx-auto  overflow-auto">
        <motion.div
          className="relative  z-10 w-full flex items-center justify-center"
          variants={containerVariants}
          initial="hidden"
          animate="visible"
        >
          <BackgroundGradient>
            <Card
              key={data.id}
              text={data.title}
              image={data.image}
              guidelines={guidelines}
              isActive={cardActive}
              onClick={handleCardClick}
              slug={slug}
            />
          </BackgroundGradient>
        </motion.div>
      </div>
      <BackgroundBeams />
    </>
  );
};

export default LuxuryCard;
