
"use client"
import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import NavBar from '@/components/Navbar';
import { urlForImage } from '@/lib/image';

const ProjectCard = ({blog,url}) => {

  return (
    <>
      <NavBar />

      <div className="flex-grow">
        
        {/* <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4"> */}
          
            <Link href={`/${url}/${blog.slug}`} >
            <div  className='group border h-96 p-9'>
            <div className="w-full h-[150px] relative">
                  <Image src={urlForImage(blog.image)} alt={blog.title} layout="fill" objectFit="cover" />
                </div>
              <div className="group-hover:text-[#04ADEF] text-lg leading-snug font-sans transition duration-300 mt-5 font-light hover:text-blue-700">
                {blog.title}
              </div>
              <div className="mt-5 font-light hover:text-[#04ADEF]">
                {blog.location} | {blog.projectStatus} {blog.completionYear && `| ${blog.completionYear}`}
              </div>
              <div className="flex flex-wrap font-light hover:text-[#04ADEF]">
                {blog?.tags?.map((tag, index) => (
                  <div key={index} className="font-light hover:text-[#04ADEF]">
                    #{tag}
                  </div>
                ))}
              </div>
            </div>
            </Link>
          
        {/* </section> */}
      </div>
    </>
  );
};

export default ProjectCard;
