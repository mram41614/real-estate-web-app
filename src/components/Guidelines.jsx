"use client"
import React, { useState } from 'react'
import { Button } from "@/components/ui/moving-border";
import Link from 'next/link';

const Guidelines = () => {
  const [showMore, setShowMore] = useState(false);


  return (
    <div>
        <h1 className="text-[1.5rem] merriweather mx-4 font-bold text-center mt-5 mb-3 text-gray-500">GUIDELINES</h1>
        <section className="mx-6  px-4 text-[1.1rem] work_sans font-normal leading-snug">
          <div className="relative  flex flex-col h-full min-w-0 p-4 break-words bg-white shadow-soft-xl bg-clip-border">
            <div className="relative h-full overflow-hidden bg-white rounded-xl" >
              <div className="relative z-10 border-black p-5 border-4 rounded-lg  flex flex-col flex-auto h-full ">
                <div className=''>
                  <div className="text-gray-500 works_sans font-light mb-0">Regalium is not merely a structure, but a meticulously orchestrated environment where diverse industries converge: retail emporiums, bustling offices, delectable epicurean destinations, and a haven of opulent hospitality. Within its walls lies the potential for unparalleled success. To ensure a harmonious and flourishing ecosystem, we present these Tenant Guidelines.
                  </div>
                  <br />

<div className='text-gray-500 font-light -mt-2'>Consider these guidelines your strategic roadmap. Meticulously crafted, they offer a comprehensive overview of essential protocols and best practices. Navigate seamlessly through this multifaceted masterpiece, ensuring your brand resonates within Regalium's vibrant tapestry.</div>
                </div>
                <div className="flex justify-center mt-4">
                  <Button
                    borderRadius="1.75rem"
                    className="bg-white work_sans font-light border-4 hover:text-blue-400 dark:bg-slate-900 text-black dark:text-white border-neutral-200 dark:border-slate-800"
                    onClick={()=>setShowMore(!showMore)}
                  >
                    {showMore? 'Read Less':'Read More'}
                  </Button>
                </div>
                {showMore==true && (
                  <div className="flex justify-around mt-4">
                    <Link href="/guide/common">
                    <Button
                      borderRadius="1.75rem"
                      className="bg-white border-4 hover:text-blue-400 dark:bg-slate-900 text-black dark:text-white border-neutral-200 dark:border-slate-800"
                      
                    >
                      Common
                    </Button>
                    </Link>
                    <Link href="/guide/retail">
                    <Button
                      borderRadius="1.75rem"
                      className="bg-white border-4 hover:text-blue-400 dark:bg-slate-900 text-black dark:text-white border-neutral-200 dark:border-slate-800"
                      
                    >
                      Retail
                    </Button>
                    </Link>
                    <Link href="/guide/office">
                    <Button
                      borderRadius="1.75rem"
                      className="bg-white border-4 hover:text-blue-400 dark:bg-slate-900 text-black dark:text-white border-neutral-200 dark:border-slate-800"
                      
                    >
                      Office
                    </Button>
                    </Link>
                    <Link href="/guide/fnb">
                    <Button
                      borderRadius="1.75rem"
                      className="bg-white border-4 hover:text-blue-400 dark:bg-slate-900 text-black dark:text-white border-neutral-200 dark:border-slate-800"
                      
                    >
                      F&B
                    </Button>
                    </Link>
                    <Link href="/guide/hotel">
                    <Button
                      borderRadius="1.75rem"
                      className="bg-white border-4 hover:text-blue-400 dark:bg-slate-900 text-black dark:text-white border-neutral-200 dark:border-slate-800"
                      
                    >
                      Hotel
                    </Button>
                    </Link>
                  </div>
                )}
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default Guidelines;
