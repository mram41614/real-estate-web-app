// import React from 'react'
// import Link from 'next/link'

// const Footer = () => {
//   return (
//     <>
//       <div className='bg-[#A46C4E] p-12 text-gray-500 merriweather'>
//         <div className='flex flex-col md:flex-row justify-around items-center'>
//           <div>
//             <div className='text-center md:text-left md:ml-28 text-3xl text-white mb-4 md:mb-0'>
//               Connect With Machani Group
//             </div>
//           </div>       
//           <div className='flex justify-between'>
//             <Link href='/contact-us'>
//               <button className="bg-white text-[#A46C4E] font-semibold py-2 px-4 border border-blue-500 hover:border-transparent rounded">
//                 Contact Us
//               </button>
//             </Link>
//             <a href="https://real-state-cms.sanity.studio/structure" className='ml-12' target='_blank' >
//                 <button className=" bg-gray-600 text-white font-semibold py-2 px-4 border border-blue-500 hover:border-transparent rounded">
//                     Create
//                 </button>
//             </a>
//             <Link href='/login'>
//                 <button className="bg-green-500 text-white ml-12 font-semibold py-2 px-4 border border-blue-500 hover:border-transparent rounded">
//                   Tenant Login
//                 </button>
//               </Link>
//           </div>
//         </div>
//       </div>
//       <div className='p-3 text-center text-gray-500 merriweather   md:text-left md:ml-10'>
//         <div>© 2023 DHI Design</div>
//       </div>
//     </>
//   )
// }


"use client"
import React from 'react'
import { useEffect, useState } from 'react'
import { client } from '@/lib/client'
import Image from 'next/image'
import Link from 'next/link'
import { FaXTwitter } from "react-icons/fa6";
import { FaFacebookF } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa6";
import { usePathname } from "next/navigation";
import {
  ClerkProvider,
  SignInButton,
  SignedIn,
  SignedOut,
  UserButton
} from '@clerk/nextjs'



const Footer = () => {
  const [sidePanel, setSidePanel] = useState(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [guidelinesOpen, setGuidelinesOpen] = useState(false); // New state for guidelines toggle
  const pathname = usePathname();

  const guidelineLinks = [
    { title: "Common", href: "/guide/common" },
    { title: "Retail", href: "/guide/retail" },
    { title: "Office", href: "/guide/office" },
    { title: "F&B", href: "/guide/fnb" },
    { title: "Hotel", href: "/guide/hotel" }
  ];

  useEffect(() => {
    const fetchData = async () => {
      try {
        const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
          sidePanelLinks,
          socialMediaLinks,
        }`;

        const [sidePanelData] = await Promise.all([
          client.fetch(sidePanelQuery),
        ]);

        setSidePanel(sidePanelData[0]);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const toggleGuidelines = () => {
    setGuidelinesOpen(!guidelinesOpen);
  };

  return (
    <div className='bg-[#F0F0F0] p-10 h-auto w-full work_sans font-light'>
      <div className='flex flex-col md:flex-row justify-between mb-16'>
        <div className='flex flex-col md:flex-row'>
          <div className="flex flex-col items-center">
            <div className="flex flex-col text-md items-start w-full md:w-auto gap-5 p-5">
              {/* Sidebar links for larger devices */}
              <div className="hidden md:flex flex-col gap-2 w-full">
                {sidePanel?.sidePanelLinks?.map((link, index) => (
                  <a key={index} href={link.url} className={`hover:text-[#04ADEF] ${pathname.startsWith(link.url) ? 'text-blue-500' : 'text-black'}`}>
                    {link.title}
                  </a>
                ))}
                <div>
                  <button onClick={toggleGuidelines} className="text-black hover:text-[#04ADEF]">
                    Guidelines
                  </button>
                  {guidelinesOpen && (
                    <div className="ml-4 mt-2 flex flex-col gap-2">
                      {guidelineLinks.map((link, index) => (
                        <a key={index} href={`${link.href}`} className="text-black hover:text-[#04ADEF]">
                          {link.title}
                        </a>
                      ))}
                    </div>
                  )}
                </div>
              </div>

              {/* Sidebar links for smaller devices */}
              <div className="flex md:hidden w-full flex-col gap-2">
                <div className="flex justify-between w-full">
                  {sidePanel?.sidePanelLinks?.slice(0, 1).map((link, index) => (
                    <a key={index} href={link.url} className="text-black hover:text-[#04ADEF] text-center">
                      {link.title}
                    </a>
                  ))}
                  <div className="relative inline-block text-left">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex justify-center w-auto rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                      More
                    </button>
                    {dropdownOpen && (
                      <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <div className="py-1">
                          {sidePanel?.sidePanelLinks?.slice(1).map((link, index) => (
                            <a key={index} href={link.url} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                              {link.title}
                            </a>
                          ))}
                          <div>
                            <button onClick={toggleGuidelines} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                              Guidelines
                            </button>
                            {guidelinesOpen && (
                              <div className="ml-4 mt-2 flex flex-col gap-2">
                                {guidelineLinks.map((link, index) => (
                                  <a key={index} href={`${link.href}`} className="work_sans text-gray-700 hover:text-[#04ADEF]">
                                    {link.title}
                                  </a>
                                ))}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='ml-0 md:ml-12 p-4'>
            <div className='font-bold text-gray-400'>
              More ways to explore
            </div>
            <div className='hover:text-[#04ADEF]'>
              <Link href='/contact-us'>
              Job Opportunities
              </Link>
              </div>
            <div>
              <Link href="/contact-us" className="hover:text-[#04ADEF]">
                Contact-Us
              </Link>
            </div>
            <div className='hover:text-[#04ADEF]'>
              {/* <Link href="/login">
                Tenant Login
              </Link> */}
              <SignedOut>
          <SignInButton >
         <button>Tenant Login</button> 
             </ SignInButton>
          
        </SignedOut>
        <SignedIn>
          <UserButton />
        </SignedIn>
            </div>
          </div>
        </div>
        <div className='mt-8 md:mt-0 md:mr-12'>
          <div className="flex flex-row w-full justify-center md:justify-start gap-4">
            {sidePanel?.socialMediaLinks?.map((link, index) => {
              let icon;
              switch (link.title) {
                case 'X':
                  icon = <FaXTwitter size={18} />;
                  break;
                case 'Facebook':
                  icon = <FaFacebookF size={18} />;
                  break;
                case 'Instagram':
                  icon = <FaInstagram size={18} />;
                  break;
                case 'LinkedIn':
                  icon = <FaLinkedinIn size={18} />;
                  break;
                default:
                  icon = link.title; // Fallback to the title if no match is found
              }
              return (
                <a key={index} href={link.url} className="text-black hover:text-[#04ADEF]">
                  {icon}
                </a>
              );
            })}
          </div>
          <div className='mt-5 flex justify-center md:justify-start'>
            <Link href='/'>
            <Image 
              src='/machani-group-logo.png'
              width={100}
              height={100}
              alt="Machani Group Logo"
            />
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
