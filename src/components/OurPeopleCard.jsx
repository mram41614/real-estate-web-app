import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { urlForImage } from '@/lib/image';

const OurPeopleCard = ({ person }) => {
  return (
    <Link href={`/our-people/${person.slug}`} key={person._id}>
      <div className="bg-white rounded-lg h-[500px] shadow-md p-6 my-6 text-center transform hover:scale-105 transition-transform duration-300 flex flex-col">
        {person.imageUrl && (
          <div className="flex-shrink-0 w-32 h-32 md:w-48 md:h-48 mx-auto mb-4 relative">
            <Image
              width={192}
              height={192}
              src={urlForImage(person.imageUrl)}
              alt={person.name}
              className="rounded-full object-cover w-full h-full"
            />
          </div>
        )}
        <div className='mb-2 flex-grow'>
          <h3 className="text-lg md:text-xl font-semibold mb-2">{person.name}</h3>
          <div className="text-gray-700 text-center merriweather font-semibold">{person.role}</div>
          <hr className="w-1/2 md:w-1/3 mx-auto my-4 border-t-2 border-gray-300" />
          <div className="text-gray-700 work_sans">{person.description}</div>
        </div>
      </div>
    </Link>
  );
};

export default OurPeopleCard;
