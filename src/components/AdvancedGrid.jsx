// import Image from "next/image";
// import Link from "next/link";
// import { urlForImage } from '../lib/image';

// export default function AdvancedGrid({ blog,url ,index}) {

//   const getGridItemClass = (index) => {
//     // Customize which cards should be larger
//     if (index % 3 === 0) {
//       return 'sm:col-span-2';
//     }

//     return '';
//   };

//   const getMarginStyle = (index) => {
//     if (typeof window !== 'undefined') {
//       if (window.innerWidth >= 768 && index > 2 && index % 3 !== 0) {
//         return { marginTop: `${Math.floor(index / 3) * (-15)}rem` };
//       }
//     }
//   };

//   return (
//     <section className={`flex flex-col md:w-full group bg-white  ${getGridItemClass(index)}`}
//     style={getMarginStyle(index)}
//     >
//       <Link href={`/${url}/${blog.slug}`}> 
//       {/* Image Section */}
//       <div className={`relative  w-full  hover:border-[#04ADEF] ${index%3===0?'h-[35rem]':'h-80'}`} >
//             <Image
//               src={urlForImage(blog.image)}
//               alt="AI for everyone"
//               layout="fill"
//               objectFit="cover"            
//             />           
//             <div className="absolute bottom-0 left-8 z-20 w-0 h-0 border-l-[20px] border-l-transparent border-b-[20px] border-b-[#F9FAFB] border-r-[20px] border-r-transparent" />          
            
//       </div>
          
//           {/* Meta Information */}
//       <div className={`px-8 h-60 mb-8 group-hover:text-[#04ADEF] border-t-4 border-gray-50  hover:border-t-4 group-hover:border-[#04ADEF]`} >         
//           <div className="flex mt-2 group-hover:text-[#04ADEF]  justify-between px-0 text-gray-600 text-[1.1rem] font-light work_sans">
//             <div>{blog.tagType}</div>
//             <div>
//               {blog.publishDate && new Date(blog.publishDate).toLocaleDateString('en-US', {
//                 day: 'numeric',
//                 month: 'long',
//                 year: 'numeric',
//               })}
//             </div>
//           </div>

//           {/* Title and Summary */}
//           <div className="flex flex-col hover:text-[#04ADEF] justify-between  p-4 px-0 ">     
//             <div className="hover:text-[#04ADEF]">    
//               <div className="text-[1.5rem] group-hover:text-[#04ADEF]  merriweather text-gray-500 hover:text-[#04ADEF] font-normal line-clamp-2 hover:underline leading-tight ">
//                 {blog.title} <span className="text-gray-500 group-hover:text-[#04ADEF] font-extrabold text-[1.5rem]">{'>'}</span>
//               </div>          
//                  <div className="line-clamp-6 group-hover:text-[#04ADEF] text-gray-400   ">
//                   <div className="font-light work_sans text-[1rem] leading-snug">{blog.summary}</div>
//                 </div>
//               </div>
//               </div>  
//         </div>
//       </Link>
//     </section>
//   );
// }

// import { useEffect, useState } from "react";
// import Image from "next/image";
// import Link from "next/link";
// import { urlForImage } from '@/lib/image';

// export default function AdvancedGrid({ blog, url, index }) {
//   const [marginStyle, setMarginStyle] = useState(null);

//   useEffect(() => {
//     const handleResize = () => {
      
//       if (window.innerWidth >= 1024 && index > 2 && index % 3 !== 0) {
//         setMarginStyle({ marginTop: `${Math.floor(index / 3) * (-15)}rem` });
//       } else {
//         setMarginStyle(null);
//       }
//     };

//     handleResize(); // Set the initial style
//     window.addEventListener('resize', handleResize);

//     return () => window.removeEventListener('resize', handleResize);
//   }, [index]);

//   const getGridItemClass = (index) => {
//     // Customize which cards should be larger
    
//     if (index % 3 === 0) {
//       return 'sm:col-span-2';
//     }
//     return 'sm:col-span-1';
//   };

//   return (
//     <section className={`flex flex-col md:w-full group bg-white ${getGridItemClass(index)}`} style={marginStyle}>
//       <Link href={`/${url}/${blog?.slug}`}>
//         {/* Image Section */}
//         <div className={`relative w-full hover:border-[#04ADEF] ${index % 3 === 0 ? 'h-[35rem]' : 'h-80'}`}>
//           {blog?.image && (
//             <Image
//               src={urlForImage(blog.image)}
//               alt="AI for everyone"
//               layout="fill"
//               objectFit="cover"
//             />
//           )}
//           <div className="absolute bottom-0 left-8 z-20 w-0 h-0 border-l-[20px] border-l-transparent border-b-[20px] border-b-[#F9FAFB] border-r-[20px] border-r-transparent" />
//         </div>

//         {/* Meta Information */}
//         <div className={`px-4 py-2 md:px-8 h-60 mb-8 group-hover:text-[#04ADEF] border-t-4 border-gray-50 hover:border-t-4 group-hover:border-[#04ADEF]`}>
//           <div className="flex group-hover:text-[#04ADEF] justify-between text-gray-600 text-[0.8rem] font-light work_sans">
//             <div>{blog?.tagType}</div>
//             <div>
//               {blog?.publishDate && new Date(blog.publishDate).toLocaleDateString('en-US', {
//                 day: 'numeric',
//                 month: 'long',
//                 year: 'numeric',
//               })}
//             </div>
//           </div>

//           {/* Title and Summary */}
//           <div className="flex flex-col hover:text-[#04ADEF] justify-between mt-1">
//             <div className="hover:text-[#04ADEF]">
//               <div className="text-[1.5rem] group-hover:text-[#04ADEF] merriweather text-gray-500 hover:text-[#04ADEF] font-normal line-clamp-2 hover:underline leading-tight">
//                 {blog?.title} <span className="text-gray-500 group-hover:text-[#04ADEF] font-extrabold text-[1.5rem]">{'>'}</span>
//               </div>
//               <div className="line-clamp-6 group-hover:text-[#04ADEF] text-gray-400 mt-2">
//                 <div className="font-light work_sans text-[1rem] leading-snug">{blog?.summary}</div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </Link>
//     </section>
//   );
// }

import { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { urlForImage } from '@/lib/image';

export default function AdvancedGrid({ blog, url, index }) {
  const [marginStyle, setMarginStyle] = useState(null);

  useEffect(() => {
    const handleResize = () => {
      const cycleIndex = index;
      if (window.innerWidth >= 1024 && cycleIndex > 2 && cycleIndex % 3 !== 0) {
        setMarginStyle({ marginTop: `${Math.floor(cycleIndex / 3) * (-15)}rem` });
      } else {
        setMarginStyle(null);
      }
    };

    handleResize(); // Set the initial style
    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, [index]);

  const getGridItemClass = (index) => {
    const cycleIndex = index;
    // Customize which cards should be larger
    if (cycleIndex % 3 === 0) {
      return 'sm:col-span-2';
    }
    return 'sm:col-span-1'; // Ensure smaller cards span 1 column
  };

  return (
    <section className={`flex flex-col md:w-full group bg-white ${getGridItemClass(index)}`} style={marginStyle}>
      <Link href={`/${url}/${blog?.slug}`}>
        {/* Image Section */}
        <div className={`relative w-full hover:border-[#04ADEF] ${index % 3 === 0 ? 'h-[35rem]' : 'h-80'}`}>
          {blog?.image && (
            <Image
              src={urlForImage(blog.image)}
              alt="AI for everyone"
              layout="fill"
              objectFit="cover"
            />
          )}
          <div className="absolute bottom-0 left-8 z-20 w-0 h-0 border-l-[20px] border-l-transparent border-b-[20px] border-b-[#F9FAFB] border-r-[20px] border-r-transparent" />
        </div>

        {/* Meta Information */}
        <div className={`px-4 py-2 md:px-8 h-60 mb-8 group-hover:text-[#04ADEF] border-t-4 border-gray-50 hover:border-t-4 group-hover:border-[#04ADEF]`}>
          <div className="flex group-hover:text-[#04ADEF] justify-between text-gray-600 text-[0.8rem] font-light work_sans">
            <div>{blog?.tagType}</div>
            <div>
              {blog?.publishDate && new Date(blog.publishDate).toLocaleDateString('en-US', {
                day: 'numeric',
                month: 'long',
                year: 'numeric',
              })}
            </div>
          </div>

          {/* Title and Summary */}
          <div className="flex flex-col hover:text-[#04ADEF] justify-between mt-1">
            <div className="hover:text-[#04ADEF]">
              <div className="text-[1.5rem] group-hover:text-[#04ADEF] merriweather text-gray-500 hover:text-[#04ADEF] font-normal line-clamp-2 hover:underline leading-tight">
                {blog?.title} <span className="text-gray-500 group-hover:text-[#04ADEF] font-extrabold text-[1.5rem]">{'>'}</span>
              </div>
              <div className="line-clamp-6 group-hover:text-[#04ADEF] text-gray-400 mt-2">
                <div className="font-light work_sans text-[1rem] leading-snug">{blog?.summary}</div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </section>
  );
}

