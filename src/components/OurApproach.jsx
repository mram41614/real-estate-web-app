import React from 'react';
import Image from 'next/image';

const OurApproach = () => {
  return (
    <div className='mt-16' id='who-we-are'>
      <div className="bg-white py-10">
        <h1 className="text-center merriweather text-[1.5rem]  font-bold text-gray-500 mb-36">Who We Are</h1>
      </div>
      <div className="bg-gray-500 w-full text-center pt-20 pb-10">
      <div className="h-96 max-w-6xl mx-auto -mt-60 relative">
            <Image
              src="/frontpage.webp"
              layout="fill"
              objectFit="cover"
              alt="Our Approach Image"
            />
          </div>
      
      
        
          
          <div className="rounded-lg mt-10 overflow-hidden  bg-blue-gray-500/40  p-8 md:p-12 lg:p-16 text-white">
            <h2 className=" merriweather text-[1.5rem]  font-normal mb-4">
            Commited to delivering a functional, aesthetic & bespoke product for you
            </h2>
            <p className="text-[1.1rem] work_sans leading-snug">
            Existing within the luxury retail and design realm, our studio aims to bring to light the finer things in life and living for our clients. Working closely with our clients, our team understands their aesthetic and functional needs, and helps provide a seamless experience of highly customisable, detail-oriented and personalised design solutions that are tailored to their lifestyle and requirements. The overall approach of our studio, be it in terms of services, products or even our internal structure, is that of a mindful one where everything has a purpose - we believe in creating spaces that are comfortable, inviting, and inspirational just like the people who inhabit them.            </p>
          </div>
        
      </div>
    </div>
  );
};

export default OurApproach;
