import React from 'react';
import Link from 'next/link';

const BelowHero = () => {
  return (
    <div className="bg-[#F3E7D9] w-full mt-10 work_sans flex items-center">
      <div className="bg-[#F3E7D9] p-10  mx-auto ">
        <h2 className=" font-bold text-center text-gray-500 mb-6 text-[1.5rem] merriweather">What Are You Looking To Achieve?</h2>
        <div className="text-xl w-full space-y-4 md:space-y-0 md:grid md:grid-cols-2  md:gap-x-[16rem] ">
          <div className="flex flex-col items-start py-2 border-b md:border-none border-gray-200">
            <div className="text-gray-500 ">Deliver new products to market</div>
            <Link href="/how-we-help-clients" className="text-blue-500   hover:underline text-[1rem]  font-semibold">Learn more</Link>
          </div>
          <div className="flex flex-col items-start py-2 border-b md:border-none border-gray-200">
            <div className="text-gray-500 ">Reduce cost to operate/assemble</div>
            <Link href="/how-we-help-clients" className="text-blue-500   hover:underline text-[1rem] font-semibold">Learn more</Link>
          </div>
          <div className="flex flex-col items-start py-2 border-b md:border-none border-gray-200">
            <div className="text-gray-500 ">Build a better customer experience</div>
            <Link href="/how-we-help-clients" className="text-blue-500   hover:underline text-[1rem] font-semibold">Learn more</Link>
          </div>
          <div className="flex flex-col items-start py-2 border-b md:border-none border-gray-200">
            <div className="text-gray-500 ">Increase employee engagement</div>
            <Link href="/how-we-help-clients" className="text-blue-500   hover:underline text-[1rem] font-semibold">Learn more</Link>
          </div>

        </div>
      </div>
    </div>
  );
};

export default BelowHero;
