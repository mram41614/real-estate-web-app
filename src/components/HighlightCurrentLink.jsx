"use client"
import {usePathname} from 'next/navigation';
import Link from 'next/link';

export default function HighLightCurrentLink() {
    const pathname = usePathname();

    const HorizontalLinks = [
        {
            title:'Common',
            url:'/guide/common'
        },
        {
            title:'Retail',
            url:'/guide/retail'
        },
        {
            title:'Office',
            url:'/guide/office'
        },
        {
            title:'F & B',
            url:'/guide/fnb'
        },
        {
            title:'Hotel',
            url:'/guide/hotel'
        },
      ]

      return (
        <div className='flex flex-row overflow-x-auto whitespace-nowrap merriweather ml-6 text-black font-bold'>
        {
            HorizontalLinks.map((data,index)=>(
                <Link href={data.url} key={index}>
                    <div className={`p-5 ${pathname.startsWith(data.url) ? 'text-blue-500' : ''}`}>
                    {data.title}
                    </div>               
                </Link>
            ))
        }
        </div>
      )
}