import Image from "next/image";
import { urlForImage } from '@/lib/image';

export default function GuidelineCard({ blog,url }) {

  return (
    <section className="flex flex-col md:w-full group bg-white  rounded ">
      {/* <Link href={`/${url}/${blog.slug}`} className=""> */}
      {/* Image Section */}
      <div className="relative h-80 w-full  hover:border-[#04ADEF] ">
            <Image
              src={urlForImage(blog.image)}
              alt="AI for everyone"
              layout="fill"
              objectFit="cover"            
            />           
            <div className="absolute bottom-0 left-8 z-20 w-0 h-0 border-l-[20px] border-l-transparent border-b-[20px] border-b-[#F9FAFB] border-r-[20px] border-r-transparent" />                    
      </div>
          
          {/* Meta Information */}
      <div className="px-8 h-60 mb-8 group-hover:text-[#04ADEF] border-t-4 border-gray-50  hover:border-t-4 group-hover:border-[#04ADEF]" >         

          {/* Title and Summary */}
          <div className="flex flex-col hover:text-[#04ADEF] justify-between  p-4 px-0 ">     
            <div className="hover:text-[#04ADEF]">    
              <h2 className="text-2xl group-hover:text-[#04ADEF] merriweather text-gray-500 hover:text-[#04ADEF] font-semibold line-clamp-2 hover:underline leading-tight ">
                {blog.title} <span className="text-blue-800 font-extrabold text-lg">{'>'}</span>
              </h2>          
                 <div className="line-clamp-6 group-hover:text-[#04ADEF] text-gray-400 font-sans  ">
                  <div className="font-light leading-snug">{blog?.summary}</div>
                </div>
              </div>
              </div>  
        </div>
      {/* </Link> */}
    </section>
  );
}
