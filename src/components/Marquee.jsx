// components/Marquee.js

import React from 'react';
import BlogCard from '@/components/BlogCard';

const Marquee = ({ blogs,url }) => {

  return (
    <div className="maylike-products-wrapper">
      <h2 className="text-[1.5rem] merriweather text-center font-semibold mb-4">You May Also Like</h2>
      <div className="marquee-container">
        <div className="marquee">
          {blogs.map((blog, index) => (
            <div key={index} className="marquee-item   ">
              <BlogCard blog={blog} url={url} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Marquee;
