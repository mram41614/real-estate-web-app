import { auth } from "@clerk/nextjs/server"

export const checkRole = (role) => {
  const { sessionClaims } = auth()
  

  return sessionClaims?.metadata.role === role;
// if (typeof window === "undefined") {
//     // Server-side
//     const { sessionClaims } = auth();
//     return sessionClaims && sessionClaims.metadata && sessionClaims.metadata.role === role;
//   } else {
//     // Client-side
//     const { user } = useAuth();
//     return user && user.publicMetadata && user.publicMetadata.role === role;
//   }
}