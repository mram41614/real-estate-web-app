
// 'use client'
// import Image from "next/image";
// import Navbar from "@/components/Navbar";
// import { client } from '@/lib/client';
// import { useEffect, useState } from "react";
// import Footer from "@/components/Footer";
// import { usePathname } from "next/navigation";
// import ResearchCard from "@/components/ResearchCard";
// import Link from "next/link";

// export default function Home() {
//   const [blogs, setBlogs] = useState([]);
//   const pathname = usePathname();

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const query = `*[_type=='projects'] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const [posts] = await Promise.all([
//           client.fetch(query),         
//         ]);

//         setBlogs(posts);
      
//       } catch (error) {
//         console.error("Error fetching data:", error);
//       }
//     };

//     fetchData();
//   }, []);

    

//   return (
//     <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
//       <Navbar />
//       <div className="relative w-full h-[500px]">
//         <Image
//           src='/mitbg.jpg'
//           layout="fill"
//           objectFit="cover"
//           alt='mitbg'
//           className="w-full h-full"
//         />
//       </div>   
      
//       <div className='flex flex-row overflow-x-auto whitespace-nowrap merriweather ml-6 text-black font-bold'>
//         {/* 
//            Add filters here 
//         */}
//         </div>
//       <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4">
//         {blogs.map((blog, index) => (
//             <div key={index} className={`hover:text-blue-500`}>
//               <ResearchCard blog={blog} url={'projects'} />
//             </div>
//           ))}
//       </section>    
//       <Footer />
//     </div>
//   );
// }


'use client'
import Image from "next/image";
import Navbar from "@/components/Navbar";
import { client } from '@/lib/client';
import { useEffect, useState } from "react";
import Footer from "@/components/Footer";
import { usePathname } from "next/navigation";
import ProjectCard from "@/components/ProjectCard";

export default function Home() {
  const [blogs, setBlogs] = useState([]);
  const [filteredBlogs, setFilteredBlogs] = useState([]); // State for filtered blogs
  const [selectedTagType, setSelectedTagType] = useState(''); // State for selected tag type filter
  const pathname = usePathname();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='projects'] | order(_createdAt asc) {
          title,
          image,
          tagType1,
          tagType2,
          publishDate,
          location,
          projectStatus,
          completionYear,
          summary,
          "authors": authors[]->{
            name,
            "slug": slug.current
          },
          tags,
          "slug": slug.current,
          body
        }`;

        const [posts] = await Promise.all([
          client.fetch(query),         
        ]);

        setBlogs(posts);
        setFilteredBlogs(posts); // Initialize filtered blogs with all posts
      
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  // Function to filter blogs based on selected tag type
  const handleTagTypeFilter = (tagType) => {
    const filtered = blogs.filter(blog => blog.tagType1 === tagType || blog.tagType2 === tagType);
    setFilteredBlogs(filtered);
    setSelectedTagType(tagType); // Update selected tag type state
  };

  // Function to reset filter (show all blogs)
  const resetFilter = () => {
    setFilteredBlogs(blogs);
    setSelectedTagType(''); // Reset selected tag type state
  };

  return (
    <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <Navbar />
      <div className="relative w-full h-[500px]">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
      </div>   
      
      <div className='flex flex-row overflow-x-auto whitespace-nowrap merriweather ml-6 text-black  font-bold'>
        {/* Button to reset filter */}
        <button
            className={`p-5 ${selectedTagType === '' ?'text-[#EB008C]':'text-black hover:text-[#04ADEF]'}`}
            onClick={resetFilter}
          >
            All
          </button>
        
        {/* Render filter buttons based on tag types */}
        {['Residential', 'Commercial', 'Mixed-Use', 'Boutique', 'Interiors', 'Experimental'].map((tag, index) => (
          <button
            key={index}
            className={`p-5 ${selectedTagType === tag ?'text-[#EB008C]':'text-black hover:text-[#04ADEF]'}`}
            onClick={() => handleTagTypeFilter(tag)}
          >
            {tag}
          </button>
        ))}
        
      </div>
      <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4">
        {filteredBlogs.map((blog, index) => (
            <div key={index}>
              <ProjectCard blog={blog} url={'projects'} />
            </div>
          ))}
      </section>    
      <Footer />
    </div>
  );
}
