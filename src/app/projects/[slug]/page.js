'use client'
import Image from "next/image";
import { urlForImage } from '@/lib/image';
import { client } from '@/lib/client';
import { PortableText } from "@portabletext/react";
import { useEffect, useState} from "react";
import myPortableTextComponents from "@/assets/PortableTextComponent";
import Marquee from '@/components/Marquee';
import Link from "next/link";
import FileViewer from '@/assets/FileViewer';
import Navbar from '@/components/Navbar';
import Footer from "@/components/Footer";
import {useReadingProgress} from "@/assets/ReadingProgress";
import { useRouter } from 'next/navigation';


export default function Page({ params: { slug } }) {
  const [blog, setBlog] = useState(null);
  const [blogs, setBlogs] = useState([]);
  const completion = useReadingProgress();
  const router = useRouter();


  const fetchBlog = async () => {

    try{
    const postQuery = `*[_type == 'projects' && slug.current == $slug][0] {
      title, 
      body[]{
        ...,
        "imageUrl": asset->url,
        "imageCaption": caption,
        "fileUrl": asset->url,
        _type == 'mux.video' => {
          "playbackId": asset->playbackId
        },
        _type == 'Table' => {
          rows
        }
      }, 
      video{
        asset->{
          "playbackId": playbackId
        }
      },
      tags,
      sideLinks,
      "authors": authors[]->{
        name,
        "slug": slug.current
      },
      publishDate,
      image{..., "imageUrl": asset->url}, 
      summary,
      "documentURL": document.asset->url
    }`;


  
    const postsQuery = `*[_type=='projects'] | order(_createdAt asc) {
      title,
      image,
      summary,
      publishDate,
      tagType,
      "slug": slug.current,
      body
    }`;
    

      const [data,posts] = await Promise.all([
              client.fetch(postQuery,{slug}),
              client.fetch(postsQuery)
      ]);

      setBlog(data);
      setBlogs(posts);
    }catch(error){
      console.log(error)
    }
  }

  useEffect(() => {
    fetchBlog();
    // fetchBlogs();
  }, [slug]);

  // Listen to real-time updates
  useEffect(() => {
    const subscription = client
      .listen(`*[_type == "post" && slug.current == $slug]`, { slug })
      .subscribe((update) => {
        fetchBlog(); // Fetch the latest data on update
      });

    return () => subscription.unsubscribe(); // Cleanup on unmount
  }, [slug]);

  if (!blog) {
    return (
      <div className="flex justify-center items-center  min-h-screen">
          <div className="loader"></div>
      </div>
    );
  }

  return (
    
    <article className="flex flex-col">
      <Navbar />
      {/* Featured Image */}
      <div className="relative w-full h-[500px]">
        <Image
          src={urlForImage(blog.image.imageUrl)}
          layout="fill"
          objectFit="cover"
          alt={blog.title}
          className="w-full h-full"
        />
      </div>
      
      <div className="flex flex-col md:flex-row mt-8 gap-x-8">
        {/* Side Panel */}
        <div className="flex flex-col items-start mb-5 md:w-[20%] p-12 z-20 bg-white mt-[-130px] ">
          
          <div className="">
            <hr className="my-4 border-t-2 border-gray-300" />
            {blog.publishDate && (
              <div className="text-lg text-gray-600 mb-4">
                {new Date(blog.publishDate).toLocaleDateString('en-US', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric',
                })}
              </div>
            )}
          </div>

          <div className="mb-4 w">
            <hr className="my-4 border-t-2 border-gray-300" />
            <h4 className="text-xl font-semibold mb-2">People</h4>
            {blog.authors && blog.authors.map((author, index) => (
              <div key={index} className="flex items-center gap-x-2 mt-2">
                <Link href={`/our-people/${author.slug}`}>
                  <span className="text-lg hover:text-blue-600">
                    {author.name}
                  </span>
                </Link>
              </div>
            ))}
          </div>

          <div className="mb-4 ">
            <hr className="my-4 border-t-2 border-gray-300" />
            <h4 className="text-xl font-semibold mb-2">Topics</h4>
            <div className="flex flex-col gap-2">
              {blog.tags && blog.tags.map((tag, index) => (
                <div key={index} className="text-blue-800 px-2 py-1 rounded">
                <button
                  type="button"
                  onClick={() => {
                    router.push(`/search?query=${tag}`);
                  }}
                >
                  #{tag}
                </button>
              </div>
              ))}
            </div>
          </div>

          <div className="">
            <hr className="my-4 border-t-2 border-gray-300" />
            <h4 className="text-xl font-semibold mb-2">In this Group</h4>
            <div className="flex flex-col gap-2">
              {blog.sideLinks && blog.sideLinks.map((link, index) => (
                <a key={index} href={link.url} className="text-blue-600 hover:text-blue-800">
                  {link.title}
                </a>
              ))}
            </div>
          </div>
        </div>

        {/* Blog Title and Body */}
        <div className="md:w-2/3 p-6">
        <h1 className="text-[2rem] leading-snug  line">
            {blog.title}
          </h1>

          <section className="mt-8">
            <div className="text-lg leading-normal text-dark/80 ">
              <PortableText className="leading-10" value={blog.body} components={myPortableTextComponents} />
            </div>
          </section>
        </div>
      </div>

      {/* Document Viewer */}
      <div className="flex justify-center mt-8">
        {blog.documentURL && (
          <div className="w-full h-96 p-6 md:w-2/3">
            <FileViewer url={blog.documentURL} />
          </div>
        )}
      </div>

      {/* Marquee Section */}
      <div className="flex justify-center">
        <Marquee blogs={blogs} url={'projects'} />
      </div>

      <Footer />
    </article>
  );
}
