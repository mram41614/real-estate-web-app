
'use client'
import Image from "next/image";
import Navbar from "@/components/Navbar";
import { client } from '@/lib/client';
import { useEffect, useState } from "react";
import Footer from "@/components/Footer";
import { usePathname } from "next/navigation";
import ResearchCard from "@/components/ResearchCard";
import Link from "next/link";

export default function Home() {
  const [blogs, setBlogs] = useState([]);
  const pathname = usePathname();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='researchProjects'] | order(_createdAt asc) {
          title,
          "image":image.asset->url,        
          "researchTopics":researchTopics[]->{
             title,
             "slug":slug.current
          },
          "slug":slug.current,
          summary,  
          featuredRegalium       
        }`;

        const [posts] = await Promise.all([
          client.fetch(query),
         
        ]);

        const regaliumPosts = posts.filter(blog => blog.featuredRegalium === true)

        setBlogs(regaliumPosts);
      
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  
  
  const HorizontalLinks = [
    {
        title:'Projects',
        url:'/regalium-lab'
    },
    {
        title:'Research Themes',
        url:'/regalium-lab/themes'
    },
    {
        title:'Publications',
        url:'/regalium-lab/publications'
    },  
  ]
    

  return (
    <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <Navbar />
      <div className="relative w-full h-[500px]">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
      </div>   
      
      <div className='flex flex-row overflow-x-auto whitespace-nowrap merriweather ml-6 text-black font-bold'>
        {
            HorizontalLinks.map((data,index)=>(
                <Link href={data.url} key={index}>
                    <div className={`p-5 ${pathname.startsWith(data.url) ? 'text-[#EB008C]' : 'hover:text-[#04ADEF]'}`}>
                    {data.title}
                    </div>               
                </Link>
            ))
        }
        </div>
      <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4">
        {blogs.map((blog, index) => (
            <div key={index}>
              <ResearchCard blog={blog} url={'researchandinsights/projects'} />
            </div>
          ))}
      </section>    
      <Footer />
    </div>
  );
}

