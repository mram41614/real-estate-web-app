import React from 'react'
import Image from 'next/image';
import NavBar from '@/components/Navbar';


const page = () => {
 
    const blogs = Array.from({ length: 17 }, (_, index) => ({
        id: index,
        text: `Card ${index + 1}: With insomnia, nothing's real. Everything is far away. Everything is a copy, of a copy, of a copy`,
        image: '/tmpsvg.png',
        author: 'Arghya Ghosh',
        tags:['robotics','design','network']
      }));

  return (
    <>
    <NavBar />
    <div className="flex-grow  mt-28  ">
        <section className="grid mt-20 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4 ">
          {blogs.map((blog, index) => (
            <div className='group border h-96  p-9 '>
             <div>
                <Image src={blog.image} className='ml-[-7px]' width={200} height={200} />
             </div>
            <div className="group-hover:text-[#04ADEF] text-lg leading-snug font-sans transition duration-300 mt-5 font-light hover:text-blue-700 ">
            {blog.text}
          </div>
          <div className=" mt-5 font-light hover:text-[#04ADEF] ">
            {blog.author}
          </div>
          <div className="flex flex-row font-light hover:text-[#04ADEF] ">
            {blog.tags.map((tag,index)=>(
                <div className="  font-light hover:text-[#04ADEF] ">
                #{tag}
              </div>
            ))}
          </div>
           
          </div>
          ))}
        </section>
      </div>
      </>
  )
}

export default page