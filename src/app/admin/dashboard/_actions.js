"use server";

import { checkRole } from "@/utils/roles";
import { clerkClient } from "@clerk/nextjs/server";

export async function setRole(formData) {
  // Ensure the user trying to set the role is an admin
  if (!checkRole("admin")) {
    return { status: "error", message: "Not Authorized" };
  }

  const userId = formData.get("id");
  const newRole = formData.get("role");

  if (!userId || !newRole) {
    return { status: "error", message: "Invalid data" };
  }

  try {
    const updatedUser = await clerkClient.users.updateUser(userId, {
      publicMetadata: { role: newRole },
    });
    return { status: "success", message: "Role updated successfully", data: updatedUser.publicMetadata };
  } catch (error) {
    console.error("Error updating user role:", error);
    return { status: "error", message: "Error updating role", error: error.message };
  }
}
