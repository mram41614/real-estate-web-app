// "use client";

// import { usePathname, useRouter } from "next/navigation";
// import { useState } from "react";

// export const SearchUsers = () => {
//   const router = useRouter();
//   const pathname = usePathname();
//   const [alertMessage, setAlertMessage] = useState(null);

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     const form = e.currentTarget;
//     const formData = new FormData(form);
//     const queryTerm = formData.get("search");

//     router.push(pathname + "?search=" + queryTerm);
//   };

//   return (
//     <div className="mb-6">
//       <form className="flex  items-center space-x-4" onSubmit={handleSubmit}>
//         <label htmlFor="search" className="text-lg font-medium text-gray-700">Search for Users</label>
//         <input 
//           id="search" 
//           name="search" 
//           type="text" 
//           className="border border-gray-300 rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent"
//         />
//         <button 
//           type="submit" 
//           className="bg-blue-600 text-white px-4 py-2 rounded hover:bg-blue-700 transition duration-300"
//         >
//           Submit
//         </button>
//       </form>
//       {alertMessage && (
//         <div className={`mt-4 p-4 rounded ${alertMessage.status === "success" ? "bg-green-100 text-green-700" : "bg-red-100 text-red-700"}`}>
//           {alertMessage.message}
//         </div>
//       )}
//     </div>
//   );
// };

// // The component for displaying users
// const UsersList = ({ users }) => {
//   const [alertMessage, setAlertMessage] = useState(null);

//   const handleSetRole = async (e) => {
//     e.preventDefault();
//     const form = e.currentTarget;
//     const formData = new FormData(form);

//     const response = await fetch('/api/set-role', {
//       method: 'POST',
//       body: formData,
//     }).then(res => res.json());

//     setAlertMessage({ status: response.status, message: response.message });
//   };

//   return (
//     <div className="mt-8 grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
//       {users.map((user) => (
//         <div key={user.id} className="bg-white shadow-lg rounded-lg p-6">
//           <div className="font-semibold text-xl mb-2">
//             {user.firstName} {user.lastName}
//           </div>
//           <div className="text-gray-600 mb-4">
//             {
//               user.emailAddresses.find(
//                 (email) => email.id === user.primaryEmailAddressId
//               )?.emailAddress
//             }
//           </div>
//           <div className="text-gray-500 mb-6">{user.publicMetadata.role}</div>
//           <div className="flex space-x-2">
//             <form onSubmit={handleSetRole} className="w-full">
//               <input type="hidden" value={user.id} name="id" />
//               <input type="hidden" value="admin" name="role" />
//               <button type="submit" className="bg-blue-600 text-white px-4 py-2 rounded w-full hover:bg-blue-700 transition duration-300">Make Admin</button>
//             </form>
//             <form onSubmit={handleSetRole} className="w-full">
//               <input type="hidden" value={user.id} name="id" />
//               <input type="hidden" value="moderator" name="role" />
//               <button type="submit" className="bg-green-600 text-white px-4 py-2 rounded w-full hover:bg-green-700 transition duration-300">Make Moderator</button>
//             </form>
//           </div>
//         </div>
//       ))}
//       {alertMessage && (
//         <div className={`mt-4 p-4 rounded ${alertMessage.status === "success" ? "bg-green-100 text-green-700" : "bg-red-100 text-red-700"}`}>
//           {alertMessage.message}
//         </div>
//       )}
//     </div>
//   );
// };

"use client";

import { usePathname, useRouter } from "next/navigation";
import { useState } from "react";

export const SearchUsers = () => {
  const router = useRouter();
  const pathname = usePathname();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const form = e.currentTarget;
    const formData = new FormData(form);
    const queryTerm = formData.get("search");

    router.push(pathname + "?search=" + queryTerm);
  };

  return (
    <div className="mb-6">
      <form className="flex items-center space-x-4" onSubmit={handleSubmit}>
        <label htmlFor="search" className="text-lg font-medium text-gray-700">Search for Users</label>
        <input 
          id="search" 
          name="search" 
          type="text" 
          className="border border-gray-300 rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent"
        />
        <button 
          type="submit" 
          className="bg-blue-600 text-white px-4 py-2 rounded hover:bg-blue-700 transition duration-300"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

// The component for displaying users
const UsersList = ({ initialUsers }) => {
  const [users, setUsers] = useState(initialUsers);
  const [alertMessage, setAlertMessage] = useState(null);

  const handleSetRole = async (e) => {
    e.preventDefault();
    const form = e.currentTarget;
    const formData = new FormData(form);
    const userId = formData.get("id");
    const newRole = formData.get("role");

    // Optimistically update the user role in the state
    setUsers(prevUsers =>
      prevUsers.map(user =>
        user.id === userId ? { ...user, publicMetadata: { ...user.publicMetadata, role: newRole } } : user
      )
    );

    const response = await fetch('/api/set-role', {
      method: 'POST',
      body: formData,
    }).then(res => res.json());

    setAlertMessage({ status: response.status, message: response.message });

    if (response.status !== "success") {
      // Revert the role change in case of an error
      setUsers(prevUsers =>
        prevUsers.map(user =>
          user.id === userId ? { ...user, publicMetadata: { ...user.publicMetadata, role: user.publicMetadata.role } } : user
        )
      );
    }

    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  return (
    <div className="mt-8 grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
      {users.map((user) => (
        <div key={user.id} className="bg-white shadow-lg rounded-lg p-6">
          <div className="font-semibold text-xl mb-2">
            {user.firstName} {user.lastName}
          </div>
          <div className="text-gray-600 mb-4">
            {
              user.emailAddresses.find(
                (email) => email.id === user.primaryEmailAddressId
              )?.emailAddress
            }
          </div>
          <div className="text-gray-500 mb-6">Role: {user.publicMetadata.role}</div>
          <div className="flex space-x-2">
            <form onSubmit={handleSetRole} className="w-full">
              <input type="hidden" value={user.id} name="id" />
              <input type="hidden" value="admin" name="role" />
              <button type="submit" className="bg-blue-600 text-white px-4 py-2 rounded w-full hover:bg-blue-700 transition duration-300">Make Admin</button>
            </form>
            <form onSubmit={handleSetRole} className="w-full">
              <input type="hidden" value={user.id} name="id" />
              <input type="hidden" value="moderator" name="role" />
              <button type="submit" className="bg-green-600 text-white px-4 py-2 rounded w-full hover:bg-green-700 transition duration-300">Make Moderator</button>
            </form>
          </div>
        </div>
      ))}
      {alertMessage && (
        <div className={`mt-4 p-4 rounded ${alertMessage.status === "success" ? "bg-green-100 text-green-700" : "bg-red-100 text-red-700"}`}>
          {alertMessage.message}
        </div>
      )}
    </div>
  );
};
