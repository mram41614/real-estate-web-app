// // import { redirect } from "next/navigation";
// // import { checkRole } from "@/utils/roles";
// // import { SearchUsers } from "./_search-users";
// // import { clerkClient } from "@clerk/nextjs/server";
// // import { setRole } from "./_actions";

// // export default async function AdminDashboard(params) {
// //   if (!checkRole("admin")) {
// //     redirect("/");
// //   }

// //   const query = params.searchParams?.search;

// //   const users = query ? (await clerkClient.users.getUserList({ query })).data : [];

// //   return (
// //     <>
// //       <h1>This is the admin dashboard</h1>
// //       <p>This page is restricted to users with the 'admin' role.</p>

// //       <SearchUsers />

// //       {users.map((user) => (
// //         <div key={user.id}>
// //           <div>
// //             {user.firstName} {user.lastName}
// //           </div>
// //           <div>
// //             {
// //               user.emailAddresses.find(
// //                 (email) => email.id === user.primaryEmailAddressId
// //               )?.emailAddress
// //             }
// //           </div>
// //           <div>{user.publicMetadata.role}</div>
// //           <div>
// //             <form action={setRole}>
// //               <input type="hidden" value={user.id} name="id" />
// //               <input type="hidden" value="admin" name="role" />
// //               <button type="submit">Make Admin</button>
// //             </form>
// //           </div>
// //           <div>
// //             <form action={setRole}>
// //               <input type="hidden" value={user.id} name="id" />
// //               <input type="hidden" value="moderator" name="role" />
// //               <button type="submit">Make Moderator</button>
// //             </form>
// //           </div>
// //         </div>
// //       ))}
// //     </>
// //   );
// // }


// import { redirect } from "next/navigation";
// import { checkRole } from "@/utils/roles";
// import { SearchUsers } from "./_search-users";
// import { clerkClient } from "@clerk/nextjs/server";
// import { setRole } from "./_actions";

// export default async function AdminDashboard(params) {
//   if (!checkRole("admin")) {
//     redirect("/");
//   }

//   const query = params.searchParams?.search;

//   const users = query ? (await clerkClient.users.getUserList({ query })).data : [];

//   return (
//     <div className="container flex flex-col mx-auto p-8">
//       <h1 className="text-3xl font-bold mb-6">Admin Dashboard</h1>
//       <p className="mb-6 text-lg text-gray-700">This page is restricted to users with the 'admin' role.</p>

//       <SearchUsers />

//       <div className="mt-8 flex items-center">
//         {users.map((user) => (
//           <div key={user.id} className="bg-white shadow-lg rounded-lg p-6">
//             <div className="font-semibold text-xl mb-2">
//               {user.firstName} {user.lastName}
//             </div>
//             <div className="text-gray-600 mb-4">
//               {
//                 user.emailAddresses.find(
//                   (email) => email.id === user.primaryEmailAddressId
//                 )?.emailAddress
//               }
//             </div>
//             <div className="text-gray-500 mb-6">Role : {user.publicMetadata.role}</div>
//             <div className="flex space-x-2">
//               <form action={setRole} >
//                 <input type="hidden" value={user.id} name="id" />
//                 <input type="hidden" value="admin" name="role" />
//                 <button type="submit" className="bg-blue-600 text-white px-4 py-2 rounded w-full hover:bg-blue-700 transition duration-300">Make Admin</button>
//               </form>
//               <form action={setRole} >
//                 <input type="hidden" value={user.id} name="id" />
//                 <input type="hidden" value="moderator" name="role" />
//                 <button type="submit" className="bg-green-600 text-white px-4 py-2 rounded w-full hover:bg-green-700 transition duration-300">Make Moderator</button>
//               </form>
//             </div>
//           </div>
//         ))}
//       </div>
//     </div>
//   );
// }

import { redirect } from "next/navigation";
import { checkRole } from "@/utils/roles";
import { SearchUsers } from "./_search-users";
import { clerkClient } from "@clerk/nextjs/server";
import { setRole } from "./_actions";

export default async function AdminDashboard(params) {
  if (!checkRole("admin")) {
    redirect("/");
  }

  const query = params.searchParams?.search;

  const users = query ? (await clerkClient.users.getUserList({ query })).data : [];

  console.log("Retrieved users:", users); // Debug log to check the data structure

  return (
    <div className="container flex flex-col mx-auto p-8">
      <h1 className="text-3xl font-bold mb-6">Admin Dashboard</h1>
      <p className="mb-6 text-lg text-gray-700">This page is restricted to users with the 'admin' role.</p>

      <SearchUsers />

      <div className="mt-8 grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
        {users.map((user) => (
          <div key={user.id} className="bg-white shadow-lg rounded-lg p-6">
            <div className="font-semibold text-xl mb-2">
              {user.firstName} {user.lastName}
            </div>
            <div className="text-gray-600 mb-4">
              {
                user.emailAddresses.find(
                  (email) => email.id === user.primaryEmailAddressId
                )?.emailAddress
              }
            </div>
            <div className="text-gray-500 mb-6">Role: {user.publicMetadata.role}</div>
            <div className="flex space-x-2">
              <form action={setRole}>
                <input type="hidden" value={user.id} name="id" />
                <input type="hidden" value="admin" name="role" />
                <button type="submit" className="bg-blue-600 text-white px-4 py-2 rounded w-full hover:bg-blue-700 transition duration-300">Make Admin</button>
              </form>
              <form action={setRole}>
                <input type="hidden" value={user.id} name="id" />
                <input type="hidden" value="moderator" name="role" />
                <button type="submit" className="bg-green-600 text-white px-4 py-2 rounded w-full hover:bg-green-700 transition duration-300">Make Moderator</button>
              </form>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

