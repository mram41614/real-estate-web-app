

'use client'

import { useEffect, useState } from "react";
import { useSearchParams } from "next/navigation";
import Navbar from "@/components/Navbar";
import BlogCard from "@/components/BlogCard";
import OurInsightCard from "@/components/OurInsight";
import OurPeopleCard from "@/components/OurPeopleCard";
import { client } from '@/lib/client';
import Footer from "@/components/Footer";
import ResearchCard from "@/components/ResearchCard";
import ProjectCard from "@/components/ProjectCard";

export default function SearchPage() {
  const [blogs, setBlogs] = useState([]);
  const [research, setResearch] = useState([]);
  const [researchProjects, setResearchProjects] = useState([]);
  const [projects,setProjects] = useState([]);
  const [newsandupdates,setNewsAndUpdates] = useState([]);
  const [people, setPeople] = useState([]);
  const [loading, setLoading] = useState(false);  // Loading state
  const searchParams = useSearchParams();
  const query = searchParams.get('query');

  useEffect(() => {
    if (!query) return;

    const fetchData = async () => {
      setLoading(true);  // Start loading
      try {
        const postQuery = `*[_type == 'post' && (title match $query || tagType match $query || body match $query)] | order(_createdAt asc) {
          title,
          image,
          tagType,
          publishDate,
          featured,
          summary,
          "slug": slug.current,
          body
        }`;

        const researchQuery = `*[_type == 'research' && (title match $query || summary match $query || researchTopics[]->title match $query || body match $query)] | order(_createdAt asc) {
          title,
          image,
          summary,
          "slug": slug.current,
        }`;

        const researchProjectsQuery = `*[_type == 'researchProjects' && (title match $query || summary match $query || researchTopics[]->title match $query || body match $query)] | order(_createdAt asc) {
          title,
          image,
          summary,
          "slug": slug.current,
        }`;

        const projectQuery = `*[_type=='projects' && (title match $query || tags match $query || location match $query || completionYear match $query || projectStatus match $query || tagType1 match $query || tagType2 match $query || body match $query) ] | order(_createdAt asc) {
          title,
          image,
          tagType1,
          tagType2,
          publishDate,
          location,
          projectStatus,
          completionYear,
          summary,
          "authors": authors[]->{
            name,
            "slug": slug.current
          },
          tags,
          "slug": slug.current,
          body
        }`;

        const newsandupdatesQuery = `*[_type == 'newsandupdates' && (title match $query || tagType match $query || body match $query)] | order(_createdAt asc) {
          title,
          image,
          tagType,
          publishDate,
          featured,
          summary,
          "slug": slug.current,
          body
        }`;

        const personQuery = `*[_type == 'person' && (name match $query || role match $query || body match $query || tagType match $query || description match $query)] | order(_createdAt asc) {
          name,
          "imageUrl": image.asset->url,
          role,
          body,
          order,
          description,
          "slug": slug.current
        }`;

        const [posts, researchs, researchProjects, projects, newsandupdates, people] = await Promise.all([
          client.fetch(postQuery, { query: `${query}*` }),
          client.fetch(researchQuery, { query: `${query}*` }),
          client.fetch(researchProjectsQuery,{query : `${query}*`}),
          client.fetch(projectQuery,{query:`${query}*`}),
          client.fetch(newsandupdatesQuery,{query:`${query}*`}),
          client.fetch(personQuery, { query: `${query}*` })
        ]);

        setBlogs(posts);
        setResearch(researchs);
        setResearchProjects(researchProjects);
        setProjects(projects);
        setNewsAndUpdates(newsandupdates);
        setPeople(people);

      } catch (error) {
        console.error("Error fetching data:", error);
      } finally {
        setLoading(false);  // End loading
      }
    };

    fetchData();
  }, [query]);

  return (
    <div className="min-h-screen ">
      <Navbar />
      <div className="flex flex-col p-16 mx-auto my-10">
        <h1 className="text-4xl font-extrabold text-center mb-12 text-[#ACACAC]">Search Results for "{query}"</h1>
        
        {loading ? (
          <div className="flex justify-center">
            <div className="loader"></div>
          </div>
        ) : (
          <div className="text-[#ACACAC]">
            {blogs.length > 0 && (
              <div>
                <h2 className="text-3xl font-bold mb-8 text-center ">The Machani Journal</h2>
                <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
                  {blogs.map((blog, index) => (
                    <div key={index} className="hover:text-[#04ADEF]">
                      <BlogCard blog={blog} url={'the-machani-journal'} key={index} />
                    </div>
                  ))}
                </section>
              </div>
            )}

            {research.length > 0 && (
              <div className="">
                <h2 className="text-3xl font-bold mb-8 text-center">Research</h2>
                <section className="flex justify-start w-96">
                  {research.map((post, index) => (
                    <ResearchCard blog={post} url='researchandinsights' key={index} />
                  ))}
                </section>
              </div>
            )}

            {researchProjects.length > 0 && (
              <div className="">
                <h2 className="text-3xl font-bold mb-8 text-center">Research Projects</h2>
                <section className="flex justify-start w-96">
                  {researchProjects.map((post, index) => (
                    <ResearchCard blog={post} url='researchandinsights/projects' key={index} />
                  ))}
                </section>
              </div>
            )}

            {projects.length > 0 && (
              <div className="">
                <h2 className="text-3xl font-bold mb-8 text-center ">Projects</h2>
                <section className="flex justify-start w-96">
                  {projects.map((project, index) => (
                    <ProjectCard blog={project} url='projects' key={index} />
                  ))}
                </section>
              </div>
            )}

            {newsandupdates.length > 0 && (
              <div>
                <h2 className="text-3xl font-bold mb-8 text-center">News & Updates</h2>
                <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
                  {newsandupdates.map((news, index) => (
                    <div key={index} className="hover:text-[#04ADEF]">
                      <BlogCard blog={news} url={'news-and-updates'} key={index} />
                    </div>
                  ))}
                </section>
              </div>
            )}

            {people.length > 0 && (
              <div className="">
                <h2 className="text-3xl font-bold mb-8 text-center">People</h2>
                <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
                  {people.map((person, index) => (
                    <OurPeopleCard person={person} key={index} />
                  ))}
                </section>
              </div>
            )}

            {(blogs.length === 0 && research.length === 0 && researchProjects.length === 0 && projects.length === 0 && newsandupdates.length === 0 && people.length === 0) && (
              <div className="text-center mt-16">
                <h2 className="text-2xl font-bold text-gray-600">No results found for "{query}"</h2>
              </div>
            )}
          </div>
        )}
      </div>
      <Footer />
    </div>
  );
}




// 'use client'

// import { useEffect, useState } from "react";
// import { useSearchParams } from "next/navigation";
// import Navbar from "../../components/Navbar";
// import BlogCard from "@/components/BlogCard";
// import OurInsightCard from "@/components/OurInsight";
// import OurPeopleCard from "@/components/OurPeopleCard";
// import { client } from '../../lib/client';
// import Footer from "@/components/Footer";

// export default function SearchPage() {
//   const [blogs, setBlogs] = useState([]);
//   const [insights, setInsights] = useState([]);
//   const [people, setPeople] = useState([]);
//   const [guidelinesCommon, setGuidelinesCommon] = useState([]);
//   const [guidelinesRetail, setGuidelinesRetail] = useState([]);
//   const [guidelinesOffice, setGuidelinesOffice] = useState([]);
//   const [guidelinesFnb, setGuidelinesFnb] = useState([]);
//   const [guidelinesHotel, setGuidelinesHotel] = useState([]);
//   const [loading, setLoading] = useState(false);  // Loading state
//   const searchParams = useSearchParams();
//   const query = searchParams.get('query');

//   useEffect(() => {
//     if (!query) return;

//     const fetchData = async () => {
//       setLoading(true);  // Start loading
//       try {
//         const postQuery = `*[_type=='post' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const insightQuery = `*[_type=='insight' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const personQuery = `*[_type == 'person' && (name match '${query}*' || role match '${query}' || body match '${query}' || tagType match '${query}' || description match '${query}*')] | order(_createdAt asc) {
//           name,
//           "imageUrl": image.asset->url,
//           role,
//           body,
//           order,
//           description,
//           "slug": slug.current
//         }`;

//         const guidelinesCommonQuery = `*[_type=='guidelinesCommon' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const guidelinesRetailQuery = `*[_type=='guidelinesRetail' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const guidelinesOfficeQuery = `*[_type=='guidelinesOffice' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const guidelinesFnbQuery = `*[_type=='guidelinesFnb' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const guidelinesHotelQuery = `*[_type=='guidelinesHotel' && (title match '${query}*' || tagType match '${query}' || body match '${query}*')] | order(_createdAt asc) {
//           title,
//           image,
//           tagType,
//           publishDate,
//           featured,
//           summary,
//           "slug": slug.current,
//           body
//         }`;

//         const [posts, insights, people, commonGuidelines, retailGuidelines, officeGuidelines, fnbGuidelines, hotelGuidelines] = await Promise.all([
//           client.fetch(postQuery),
//           client.fetch(insightQuery),
//           client.fetch(personQuery),
//           client.fetch(guidelinesCommonQuery),
//           client.fetch(guidelinesRetailQuery),
//           client.fetch(guidelinesOfficeQuery),
//           client.fetch(guidelinesFnbQuery),
//           client.fetch(guidelinesHotelQuery)
//         ]);

//         setBlogs(posts);
//         setInsights(insights);
//         setPeople(people);
//         setGuidelinesCommon(commonGuidelines);
//         setGuidelinesRetail(retailGuidelines);
//         setGuidelinesOffice(officeGuidelines);
//         setGuidelinesFnb(fnbGuidelines);
//         setGuidelinesHotel(hotelGuidelines);

//       } catch (error) {
//         console.error("Error fetching data:", error);
//       } finally {
//         setLoading(false);  // End loading
//       }
//     };

//     fetchData();
//   }, [query]);

//   return (
//     <div className="min-h-screen">
//       <Navbar />
//       <div className="flex flex-col p-16 mx-auto my-10">
//         <h1 className="text-4xl font-extrabold text-center mb-12">Search Results for "{query}"</h1>
        
//         {loading ? (
//           <div className="flex justify-center">
//             <div className="loader"></div>
//           </div>
//         ) : (
//           <>
//             {blogs.length > 0 && (
//               <div>
//                 <h2 className="text-3xl font-bold mb-8 text-center text-indigo-600">Posts</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {blogs.map((blog, index) => (
//                     <div key={index} className="border-2 border-white hover:border-4 hover:text-blue-500 hover:border-blue-500">
//                       <BlogCard blog={blog} key={index} />
//                     </div>
//                   ))}
//                 </section>
//               </div>
//             )}

//             {insights.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-green-600">Insights</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-8">
//                   {insights.map((insight, index) => (
//                     <OurInsightCard blog={insight} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {people.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-blue-600">People</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {people.map((person, index) => (
//                     <OurPeopleCard person={person} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {guidelinesCommon.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-red-600">Common Guidelines</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {guidelinesCommon.map((guideline, index) => (
//                     <OurInsightCard blog={guideline} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {guidelinesRetail.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-yellow-600">Retail Guidelines</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {guidelinesRetail.map((guideline, index) => (
//                     <OurInsightCard blog={guideline} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {guidelinesOffice.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-orange-600">Office Guidelines</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {guidelinesOffice.map((guideline, index) => (
//                     <OurInsightCard blog={guideline} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {guidelinesFnb.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-green -600">Food & Beverage Guidelines</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {guidelinesFnb.map((guideline, index) => (
//                     <OurInsightCard blog={guideline} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {guidelinesHotel.length > 0 && (
//               <div className="">
//                 <h2 className="text-3xl font-bold mb-8 text-center text-blue-600">Hotel Guidelines</h2>
//                 <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
//                   {guidelinesHotel.map((guideline, index) => (
//                     <OurInsightCard blog={guideline} key={index} />
//                   ))}
//                 </section>
//               </div>
//             )}

//             {(blogs.length === 0 && insights.length === 0 && people.length === 0 && guidelinesCommon.length === 0 && guidelinesRetail.length === 0 && guidelinesOffice.length === 0 && guidelinesFnb.length === 0 && guidelinesHotel.length === 0) && (
//               <div className="text-center mt-16">
//                 <h2 className="text-2xl font-bold text-gray-600">No results found for "{query}"</h2>
//               </div>
//             )}
//           </>
//         )}
//       </div>
//       <Footer />
//     </div>
//   );
// }

