"use client";
import React from "react";
import Image from "next/image";
import { AnimatePresence, motion } from "framer-motion";
import NavBar from "@/components/Navbar";
import { BackgroundBeams } from "@/components/ui/background-beams";
import { BackgroundGradient } from "@/components/ui/background-gradient";

// Define dummy data with image URLs
const dummyData = Array.from({ length: 17 }, (_, index) => ({
  id: index,
  text: `Card ${index + 1}: With insomnia, nothing's real. Everything is far away. Everything is a copy, of a copy, of a copy`,
  image: "/luxury.jpeg",
}));

const cardVariants = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const containerVariants = {
  hidden: { opacity: 1 },
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: 0.1, // Reduce stagger duration
    },
  },
};

const Card = React.memo(({ text, image }) => {
  const [hovered, setHovered] = React.useState(false);

  const handleMouseMove = (e) => {
    const card = e.currentTarget;
    const rect = card.getBoundingClientRect();
    const x = e.clientX - rect.left;
    const y = e.clientY - rect.top;
    const centerX = rect.width / 2;
    const centerY = rect.height / 2;
    const rotateX = -(y - centerY) / 10;
    const rotateY = (x - centerX) / 10;

    card.style.transform = `perspective(1000px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
  };

  const handleMouseLeave = (e) => {
    const card = e.currentTarget;
    card.style.transform = "perspective(1000px) rotateX(0deg) rotateY(0deg)";
  };

  return (
    <motion.div
      className="h-96 rounded-lg flex flex-col items-center justify-center w-full gap-4 mx-auto px-8 relative overflow-hidden transition-transform duration-300"
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={(e) => {
        setHovered(false);
        handleMouseLeave(e);
      }}
      onMouseMove={handleMouseMove}
      variants={cardVariants}
    >
      <div className="absolute inset-0 z-0">
        {/* <Vortex
          backgroundColor="black"
          rangeY={800}
          particleCount={100} // Reduce particle count for performance
          baseHue={120}
          className="absolute inset-0 w-full h-full"
        /> */}
      </div>
      <p className="md:text-2xl text-2xl font-medium text-center text-white relative z-20 max-w-2xl mx-auto">
        {text}
      </p>
      <AnimatePresence>
        {hovered && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className="h-full w-full absolute inset-0 z-10"
          >
            <Image
              src={image}
              layout="fill"
              objectFit="cover"
              className="absolute inset-0 w-full h-full object-cover"
              alt="Hover Image"
            />
          </motion.div>
        )}
      </AnimatePresence>
      <div className="absolute inset-0 [mask-image:radial-gradient(400px_at_center,white,transparent)] bg-black/50 dark:bg-black/90 z-10" />
    </motion.div>
  );
});

const LuxuryCard = () => {
  return (
    <>
    <NavBar />
    <h1 className="md:text-7xl text-xl mt-20 p-5 lg:text-9xl  text-black relative z-20 bg-clip-text text-transparent bg-gradient-to-b from-neutral-200 to-neutral-600  text-center  font-bold">
        Future of luxury retail
      </h1> 
      
    <div className="mx-auto rounded-md min-h-screen overflow-auto">
      <motion.div
        className="relative z-10 w-full flex items-center justify-center"
        variants={containerVariants}
        initial="hidden"
        animate="visible"
      >
        <div className="grid grid-cols-1 mt-10 p-5 md:grid-cols-2 lg:grid-cols-4 gap-4 w-full">
          {dummyData.map((data) => (
            <BackgroundGradient>
              <Card key={data.id} text={data.text} image={data.image} />
            </BackgroundGradient>
          ))}
        </div>
      </motion.div>
    </div>
    <BackgroundBeams />
    </>
  );
};

export default LuxuryCard;
