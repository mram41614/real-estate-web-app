
import React from 'react'
import { redirect } from "next/navigation";
import { checkRole } from "@/utils/roles";

const page = () => {

    if (!checkRole("moderator")) {
        redirect("/");
      }
  return (
    <div>Hi, I am an investor !!!</div>
  )
}

export default page