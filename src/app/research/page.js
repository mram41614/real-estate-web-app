
'use client'
import Image from "next/image";
import Navbar from "@/components/Navbar";
import { client } from '../../lib/client';
import { useEffect, useState } from "react";
import Footer from "@/components/Footer";
import { usePathname } from "next/navigation";
import ResearchCard from "@/components/ResearchCard";
import Link from "next/link";

export default function Home() {
  const [blogs, setBlogs] = useState([]);
  const [sidePanel, setSidePanel] = useState(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [guidelinesOpen, setGuidelinesOpen] = useState(false); // New state for guidelines toggle
  const pathname = usePathname();


  const guidelineLinks = [
    { title: "Common", href: "/guide/common" },
    { title: "Retail", href: "/guide/retail" },
    { title: "Office", href: "/guide/office" },
    { title: "F&B", href: "/guide/fnb" },
    { title: "Hotel", href: "/guide/hotel" }
  ];
  
  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='post'] | order(_createdAt asc) {
          title,
          image,
          tagType,
          tags,
          "authors": authors[]->{
            name,
            "slug": slug.current
          },
          publishDate,
          summary,
          "slug": slug.current,
          body
        }`;

        const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
          sidePanelLinks,
          socialMediaLinks,
        }`;

        const [posts, sidePanelData] = await Promise.all([
          client.fetch(query),
          client.fetch(sidePanelQuery),
        ]);

        setBlogs(posts);
        setSidePanel(sidePanelData[0]);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const toggleGuidelines = () => {
    setGuidelinesOpen(!guidelinesOpen);
  };
  
  const HorizontalLinks = [
    {
        title:'Groups, Centers and Initiatives',
        url:'/research'
    },
    {
        title:'Research Themes',
        url:'/research/themes'
    },
    {
        title:'Projects',
        url:'/research/projects'
    },
    {
        title:'Publications',
        url:'/research/publications'
    },
    
  ]
    

  return (
    <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <Navbar />
      <div className="relative w-full h-[500px]">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
      </div>   
      {/* <HighLightCurrentLink />  */}
      <div className='flex flex-row overflow-x-auto whitespace-nowrap merriweather ml-6 text-black font-bold'>
        {
            HorizontalLinks.map((data,index)=>(
                <Link href={data.url} key={index}>
                    <div className={`p-5 ${pathname.startsWith(data.url) ? 'text-[#EB008C]' : ''}`}>
                    {data.title}
                    </div>               
                </Link>
            ))
        }
        </div>
      <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4">
        {blogs.map((blog, index) => (
            <div key={index} className={`hover:text-blue-500`}>
              <ResearchCard blog={blog} url={'research'} />
            </div>
          ))}
      </section>    
      <Footer />
    </div>
  );
}
