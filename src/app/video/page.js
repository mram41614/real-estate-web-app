// // 'use client'

// // import React, { useEffect, useState } from 'react';
// // import { client } from '../../lib/client';
// // import MuxPlayer from '@mux/mux-player-react';

// // // Function to fetch video blog posts
// // const getVideoBlogPosts = async () => {
// //   const query = `*[_type == "videoBlogPost"] {
// //     _id,
// //     title,
// //     video {
// //       asset-> {
// //         playbackId
// //       }
// //     }
// //   }`;
// //   return await client.fetch(query);
// // };

// // // Function to fetch regular posts with video content in the body
// // const getPosts = async () => {
// //   const query = `*[_type == "post"] {
// //     _id,
// //     title,
// //     body[] {
// //       ...,
// //       _type == 'mux.video' => {
// //         "playbackId": asset->playbackId
// //       }
// //     }
// //   }`;
// //   return await client.fetch(query);
// // };

// // // Main Page Component
// // const Page = () => {
// //   const [videoBlogPosts, setVideoBlogPosts] = useState([]);
// //   const [posts, setPosts] = useState([]);

// //   useEffect(() => {
// //     async function fetchData() {
// //       const videoBlogData = await getVideoBlogPosts();
// //       setVideoBlogPosts(videoBlogData);
// //       const postData = await getPosts();
// //       setPosts(postData);
// //     }
// //     fetchData();
// //   }, []);

// //   if (!videoBlogPosts.length && !posts.length) return <div>Loading...</div>;

// //   return (
// //     <div>
// //       <h1>Video Blog Posts</h1>
// //       {videoBlogPosts.map(post => (
// //         <div key={post._id}>
// //           <h2>{post.title}</h2>
// //           {post.video && post.video.asset && post.video.asset.playbackId ? (
// //             <MuxPlayer playbackId={post.video.asset.playbackId} metadata={{ video_title: post.title }} />
// //           ) : (
// //             <p>No video available</p>
// //           )}
// //         </div>
// //       ))}

// //       <h1>Posts</h1>
// //       {posts.map(post => (
// //         <div key={post._id}>
// //           <h2>{post.title}</h2>
// //           {post.body.map((block, index) => (
// //             <div key={index}>
// //               {block._type === 'mux.video' && block.playbackId ? (
// //                 <MuxPlayer playbackId={block.playbackId} metadata={{ video_title: post.title }} />
// //               ) : (
// //                 <p>{block.children && block.children.map(child => child.text).join(' ')}</p>
// //               )}
// //             </div>
// //           ))}
// //         </div>
// //       ))}
// //     </div>
// //   );
// // };

// // export default Page;
// 'use client'
// import Image from "next/image";
// import { urlForImage } from '../../lib/image';
// import { client } from '../../lib/client';
// import { PortableText } from "@portabletext/react";
// import { useEffect, useState } from "react";
// import myPortableTextComponents from "@/assets/PortableTextComponent";
// import Marquee from '@/components/Marquee';
// import Link from "next/link";
// import FileViewer from '@/assets/FileViewer';
// import Navbar from '@/components/Navbar';
// import Footer from "@/components/Footer";

// export default function Page({ params: { slug } }) {
//   const [blog, setBlog] = useState(null);
// //   const [blogs, setBlogs] = useState([]);

//   const fetchBlog = async () => {
//     const query = `*[_type == 'post' && slug.current == $slug][0] {
//       title, 
//       body[]{
//         ...,
//         "imageUrl": asset->url,
//         "fileUrl": asset->url,
//         _type == 'mux.video' => {
//           "playbackId": asset->playbackId
//         }
//       }, 
//       author->,
//       publishDate,
//       image{..., "imageUrl": asset->url}, 
//       summary,
//       "documentURL": document.asset->url
//     }`;
//     const data = await client.fetch(query, { slug });
//     setBlog(data);
//   };

// //   const fetchBlogs = async () => {
// //     const query = `*[_type=='post'] | order(_createdAt asc) {
// //       title,
// //       image,
// //       summary,
// //       "slug": slug.current,
// //       body
// //     }`;
// //     const posts = await client.fetch(query);
// //     setBlogs(posts);
// //   };

//   useEffect(() => {
//     fetchBlog();
//     // fetchBlogs();
//   }, [slug]);

//   // Listen to real-time updates
//   useEffect(() => {
//     const subscription = client
//       .listen(`*[_type == "post" && slug.current == $slug]`, { slug })
//       .subscribe((update) => {
//         fetchBlog(); // Fetch the latest data on update
//       });

//     return () => subscription.unsubscribe(); // Cleanup on unmount
//   }, [slug]);

//   if (!blog) {
//     return <div>Loading...</div>;
//   }

//   return (
//     <article className="mt-16 px-2 2xl:px-12 flex flex-col gap-y-8">
//       <Navbar/>
//       {/* Featured Image */}
//       <div className="relative w-full h-[500px]">
//         <Image
//           src={urlForImage(blog.image.imageUrl)}
//           layout="fill"
//           objectFit="cover"
//           alt={blog.title}
//           className="w-full h-full"
//         />
//       </div>
      
//       <div className="flex flex-col md:flex-row mt-8 gap-x-8">
//         {/* Date and Author */}
//         <div className="flex flex-col items-center mb-5 md:w-[15.33%]">
//           {blog.publishDate && (
//             <div className="text-lg text-gray-600 ">
//               {new Date(blog.publishDate).toLocaleDateString('en-US', {
//                 day: 'numeric',
//                 month: 'long',
//                 year: 'numeric',
//               })}
//             </div>
//           )}
          
//           {blog.author && (
//             <div className="flex items-center gap-x-2 mt-2">
              
//               <Link href={`/our-people/${blog.author.slug.current}`}>
//                 <span className="text-lg underline">
//                   {blog.author.name}
//                 </span>
//               </Link>
//             </div>
//           )}
//         </div>

//         {/* Blog Title and Body */}
//         <div className="md:w-2/3 p-6">
//           <h1 className="text-5xl font-serif lg:text-6xl font-bold text-dark ">
//             {blog.title}
//           </h1>

//           <section className="mt-8">
//             <div className="text-lg leading-normal text-dark/80 ">
//               <PortableText value={blog.body} components={myPortableTextComponents} />
//             </div>
//           </section>
//         </div>
//       </div>

//       {/* Document Viewer */}
//       <div className="flex justify-center mt-8 ">
//       {blog.documentURL && (
//         <div className="w-full h-96 p-6 md:w-2/3">
//           <FileViewer url={blog.documentURL} />
//         </div>
//       )}
//       </div>

//       {/* Marquee Section */}
//       <div className="flex justify-center">
//         <Marquee blogs={blogs} />
//       </div>

//       <Footer />
//     </article>
//   );
// }

'use client'
// 'use client'

import React, { useEffect, useState } from 'react';
import { client } from '../../lib/client';

// Function to fetch video blog posts
const getVideoBlogPosts = async () => {
  const query = `*[_type == "videoBlogPost"] {
    _id,
    vimeo {
      id
    }
  }`;
  return await client.fetch(query);
};

// Main Page Component
const Page = () => {
  const [videoBlogPosts, setVideoBlogPosts] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const videoBlogData = await getVideoBlogPosts();
      setVideoBlogPosts(videoBlogData);
    }
    fetchData();
  }, []);

  if (!videoBlogPosts.length) return <div>Loading...</div>;

  return (
    <div>
      <h1>Video Player</h1>
      {videoBlogPosts.map(post => (
        <div key={post._id} className="video-container">
          <iframe
            autoPlay
            muted
            loop
            src={`https://player.vimeo.com/video/${post.vimeo.id}?autoplay=1&muted=1&controls=0&loop=1`}
            width="640"
            height="360"
            frameBorder="0"
            allowFullScreen
          ></iframe>
        </div>
      ))}
    </div>
  );
};

export default Page;
