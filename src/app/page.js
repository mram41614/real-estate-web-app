
'use client'
import Image from "next/image";
import Navbar from "@/components/Navbar";
import { client } from '../lib/client';
import { useEffect, useState } from "react";
import BlogCard from "@/components/BlogCard";
import AdvancedGrid from "@/components/AdvancedGrid";
import Footer from "@/components/Footer";
import { FaXTwitter } from "react-icons/fa6";
import { FaFacebookF } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa6";
import { usePathname } from "next/navigation";
import { useUser } from "@clerk/clerk-react";
import Link from "next/link";
import NavBarHomePage from "@/components/NavbarHomePage";


export default  function Home() {
  const [blogs, setBlogs] = useState([]);
  const [sidePanel, setSidePanel] = useState(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [guidelinesOpen, setGuidelinesOpen] = useState(false); // New state for guidelines toggle
  const [isModerator, setIsModerator] = useState(false); 
  const pathname = usePathname();


  //  const {orgRole} = useAuth();
  //  console.log(orgRole)

  const { user } = useUser();

  // if (user && user.publicMetadata && user.publicMetadata.role) {
  //   const userRole = user.publicMetadata.role;
  //   setIsModerator(userRole==="moderator")
  //   // return <div>User Role: {userRole}</div>;
  // }

  useEffect(() => {
    if (user && user.publicMetadata && user.publicMetadata.role) {
      const userRole = user.publicMetadata.role;
      setIsModerator(userRole === "moderator");
    }
  }, [user]);


  const guidelineLinks = [
    { title: "Common", href: "/guide/common" },
    { title: "Retail", href: "/guide/retail" },
    { title: "Office", href: "/guide/office" },
    { title: "F&B", href: "/guide/fnb" },
    { title: "Hotel", href: "/guide/hotel" }
  ];
  
  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='post'] | order(_createdAt asc) {
          title,
          image,
          tagType,
          publishDate,
          summary,
          "slug": slug.current,
          body
        }`;

        const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
          sidePanelLinks,
          socialMediaLinks,
        }`;

        const [posts, sidePanelData] = await Promise.all([
          client.fetch(query),
          client.fetch(sidePanelQuery),
        ]);

        setBlogs(posts);
        setSidePanel(sidePanelData[0]);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const toggleGuidelines = () => {
    setGuidelinesOpen(!guidelinesOpen);
  };
  
    const getGridItemClass = (index) => {
      // Customize which cards should be larger
      
      if (index % 3 === 0) {
        return 'sm:col-span-2 lg:col-span-2 ';
      }
      return '';
    };

  return (
    <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <NavBarHomePage />
      <div className="relative w-full h-[500px] ">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
        <div className="absolute inset-0 bg-black opacity-25"></div>

        <div className="absolute flex flex-wrap  top-2/3 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-white text-center">
          <div className="mb-4 ">
            <a href="/about" className="text-4xl merriweather font-bold">Who we are + What we do + Why we do</a>
          </div>
        </div>
      </div>    
           {/* Blog Cards */}
        <div className={`md:w-full bg-white grid grid-cols-1 sm:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 `}>
          <div className="bg-white z-20 ">
        <div className="flex flex-col mb-5 w-full gap-4  p-4 md:p-12 z-20 bg-white mt-[-90px]">
          <div className="flex flex-col items-center">
            <div className="flex flex-col text-md items-start min-[320px]:w-full md:w-auto gap-5 p-5">
              {/* Sidebar links for larger devices */}
              <div className="hidden min-[640px]:flex flex-col gap-3 w-full">
              {/* {orgRole==="moderator"?(<Link href='/investor' className=" hover:text-[#04ADEF] font-bold">Investor</Link>):''} */}
              {/* {orgRole === "moderator" && (
                    <Link href='/investor' className="hover:text-[#04ADEF] font-bold">Investor</Link>
                  )} */}
                  {isModerator && (
                    <Link href='/investor' className="hover:text-[#04ADEF] font-bold">Investor</Link>
                  )}
                {sidePanel?.sidePanelLinks?.map((link, index) =>{
                  const strippedUrl = new URL(link.url).pathname;
                   return (
                  <a key={index} href={link.url} className={`font-sans  hover:text-[#04ADEF] font-bold ${pathname===strippedUrl?'text-[#EB008C]':'text-black'}`}>
                    {link.title}
                  </a>
                   )
                  })}
                <div>
                  <button onClick={toggleGuidelines} className="font-sans text-black hover:text-[#04ADEF] font-bold">
                    Guidelines
                  </button>
                  {guidelinesOpen && (
                    <div className="ml-4 mt-2 flex flex-col gap-2">
                      {guidelineLinks.map((link, index) => (
                        <a key={index} href={`${link.href}`} className="font-sans text-black hover:text-[#04ADEF] font-bold">
                          {link.title}
                        </a>
                      ))}
                    </div>
                  )}
                </div>
              </div>

              {/* Sidebar links for smaller devices */}
              <div className="flex min-[640px]:hidden w-full flex-col gap-2">
                <div className="flex justify-between w-full ">
                  {sidePanel?.sidePanelLinks?.slice(0, 1).map((link, index) => (
                    <a key={index} href={link.url} className="font-sans text-black hover:text-[#04ADEF] text-center font-bold">
                      {link.title}
                    </a>
                  ))}
                  <div className="relative inline-block text-left">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex justify-center w-auto rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                      More
                    </button>
                    {dropdownOpen && (
                      <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <div className="py-1">
                          {sidePanel?.sidePanelLinks?.slice(1).map((link, index) => (
                            <a key={index} href={link.url} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                              {link.title}
                            </a>
                          ))}
                          <div>
                            <button onClick={toggleGuidelines} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                              Guidelines
                            </button>
                            {guidelinesOpen && (
                              <div className="ml-4 mt-2 flex flex-col gap-2">
                                {guidelineLinks.map((link, index) => (
                                  <a key={index} href={`${link.href}`} className="work_sans text-gray-700 hover:text-[#04ADEF] ">
                                    {link.title}
                                  </a>
                                ))}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>

              <hr className="my-4 border-t-2 w-full border-gray-300" />
              <div className="flex flex-row min-[320px]:w-full justify-center md:w-auto gap-4">
                {sidePanel?.socialMediaLinks?.map((link, index) => {
                  let icon;
                  switch (link.title) {
                    case 'X':
                      icon = <FaXTwitter size={18} />;
                      break;
                    case 'Facebook':
                      icon = <FaFacebookF size={18} />;
                      break;
                    case 'Instagram':
                      icon = <FaInstagram size={18} />;
                      break;
                    case 'LinkedIn':
                      icon = <FaLinkedinIn size={18} />;
                      break;
                    default:
                      icon = link.title; // Fallback to the title if no match is found
                  }
                  return (
                    <a key={index} href={link.url} className="text-black hover:text-[#04ADEF]">
                      {icon}
                    </a>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        </div>
          {blogs && blogs.map((blog, index) => (
            <div key={index} className={`hover:text-blue-500`}>
              <BlogCard blog={blog} url={'the-machani-journal'} />
            </div>
          ))}
          
        </div>
        {/* Support Section */}
        <div className="bg-black h-[34rem] w-full flex flex-col items-center justify-center text-white">
            <div className="flex flex-col p-10">
              <div className="">
                <h1 className="text-5xl mb-3">Support The Regalium Lab</h1>
                <div className="work_sans">
                We believe that together, we can play an important role in helping people realize a <br /> better and more just future for themselves and for all.
                </div>
                <div className="mt-3">
                  <Link href='/regalium-lab'>
                  <button class="bg-transparent mr-3 merriweather font-semibold hover:text-white py-2 px-4 border-2 border-white  ">
                    Read More
                  </button>
                  </Link>
                  {/* <button class="bg-transparent mr-3 merriweather font-semibold hover:text-white py-2 px-4 border-2 border-white  ">
                    Button
                  </button>
                  <button class="bg-transparent merriweather font-semibold hover:text-white py-2 px-4 border-2 border-white  ">
                    Button
                  </button> */}
                  </div>
                </div>
            </div>
        </div>
        <div className={`md:w-full bg-white   grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4 `}>         
          {blogs && blogs.map((blog, index) => (
            <div key={index} className={`hover:text-[#04ADEF] ${getGridItemClass(index)}`}>
              {blog?.image && <AdvancedGrid blog={blog} url={'how-we-help-clients'} index={index} />}
            </div>
          ))}
        </div>
      <Footer />
    </div>
  );
}

