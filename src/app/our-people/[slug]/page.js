'use client'
import Image from "next/image";
import { urlForImage } from '../../../lib/image';
import { client } from '../../../lib/client';
import { PortableText } from "@portabletext/react";
import { useEffect, useState } from "react";
import myPortableTextComponents from "@/assets/PortableTextComponent";
import Navbar from "@/components/Navbar";
import Footer from "@/components/Footer";
import { MdEmail } from "react-icons/md";
import { useReadingProgress } from "@/assets/ReadingProgress";

export default function Page({ params: { slug } }) {
  const [person, setPerson] = useState(null);
  const completion = useReadingProgress();

  const fetchPerson = async () => {
    const query = `*[_type == 'person' && slug.current == $slug][0] {
      _id,
      name,
      body,
      role,
      email,
      expertise,
      description,
      image
    }`;
    const data = await client.fetch(query, { slug });
    setPerson(data);
  };

  useEffect(() => {
    fetchPerson();
  }, [slug]);

  // Listen to real-time updates
  useEffect(() => {
    const subscription = client
      .listen(`*[_type == "person" && slug.current == $slug]`, { slug })
      .subscribe((update) => {
        fetchPerson(); // Fetch the latest data on update
      });

    return () => subscription.unsubscribe(); // Cleanup on unmount
  }, [slug]);

  if (!person) {
    return (
      <div className="flex justify-center items-center  min-h-screen">
          <div className="loader"></div>
      </div>
    );
  }

  return (
    <article className="mt-16  flex flex-col">
      <Navbar />

      {/* Top Section */}
      <section className="flex p-6 md:p-10 mt-10 bg-[#F0F0F0] flex-col lg:flex-row gap-8 items-center">
        {/* Image on the left */}
        <div className="flex-shrink-0">
          <Image
            src={urlForImage(person.image)}
            width={200}
            height={200}
            alt={person.name}
            className="rounded-full"
          />
        </div>

        {/* Name, Social Icons, and Description on the right */}
        <div className="flex-grow text-center lg:text-left">
          <h1 className="text-3xl lg:text-4xl font-bold">
            {person.name}
          </h1>
          <a href={`mailto:${person.email}`} className="block mt-3">
            <MdEmail className="text-2xl text-blue-500 inline-block" />
          </a>
          <div className="text-base md:text-lg leading-relaxed mt-4">
            {person.description}
          </div>
        </div>
      </section>

      {/* About and Expertise Section */}
      <section className="flex flex-col lg:flex-row gap-8 mt-8">
        {/* About Section on the left */}
        <div className="flex-grow lg:w-1/2 p-6">
          <div className="text-base md:text-lg leading-relaxed mt-2">
            {person.body ? (
              <PortableText value={person.body} components={myPortableTextComponents} />
            ) : (
              <p>No information available</p>
            )}
          </div>
        </div>

        {/* Expertise Section on the right */}
        <div className="flex-grow lg:w-1/2 p-6 md:ml-12">
          <h2 className="text-xl xs:text-2xl md:text-3xl font-bold uppercase text-accentDarkPrimary">
            Expertise
          </h2>
          <div className="text-base md:text-lg leading-relaxed mt-2">
            {person.expertise ? (
              <PortableText value={person.expertise} components={myPortableTextComponents} />
            ) : (
              <p>No expertise listed</p>
            )}
          </div>
        </div>
      </section>

      <Footer />
    </article>
  );
}
