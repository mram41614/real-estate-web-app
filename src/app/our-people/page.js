'use client'

import React from 'react'
import OurPeople from '@/components/OurPeople'
import NavBar from '@/components/Navbar'
import Footer from '@/components/Footer'

const page = () => {
  return (
    <div className='flex flex-col min-h-screen bg-gray-100' >
      <NavBar />
      <div className='flex-grow'>
         <OurPeople />
      </div>
      
      <Footer />
    </div>
  )
}

export default page