
'use client'
import Image from "next/image";
import Navbar from "@/components/Navbar";
import { client } from "@/lib/client";
import { useEffect, useState } from "react";
import Footer from "@/components/Footer";
import { usePathname } from "next/navigation";
import ResearchCard from "@/components/ResearchCard";
import Link from "next/link";

export default function Home({params:{slug}}) {
  const [blogs, setBlogs] = useState([]);
  const pathname = usePathname();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='research' && slug.current == "${slug}"] | order(_createdAt asc) {
          title,
          "image":image.asset->url,        
          "researchTopics":researchTopics[]->{
             title,
             "image":image.asset->url,        
             "slug":slug.current,
             tags,  
             summary        
          },                
        }`;

        const [posts] = await Promise.all([
          client.fetch(query),         
        ]);

        if (posts.length > 0 && posts[0].researchTopics) {
          setBlogs(posts[0].researchTopics);
        }
      
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [slug]);

  
  
  const HorizontalLinks = [
    {
        title:'Groups, Centers and Initiatives',
        url:'/researchandinsights'
    },
    {
      title:'Projects',
      url:'/researchandinsights/projects'
    },
    {
        title:'Research Themes',
        url:'/researchandinsights/themes'
    },
    {
        title:'Publications',
        url:'/researchandinsights/publications'
    },  
  ]
    

  return (
    <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <Navbar />
      <div className="relative w-full h-[500px]">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
      </div>   
      
      <div className='flex flex-row overflow-x-auto whitespace-nowrap merriweather ml-6 text-black font-bold'>
        {
            HorizontalLinks.map((data,index)=>(
                <Link href={data.url} key={index}>
                    <div className={`p-5 ${data.title==='Groups, Centers and Initiatives' ? 'text-[#EB008C]' : 'hover:text-[#04ADEF]'}`}>
                    {data.title==='Groups, Centers and Initiatives'?(`${data.title}->${slug}`):data.title}
                    </div>               
                </Link>
            ))
        }
        </div>
        {}
      <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4">
        {blogs?.map((blog, index) => (
            <div key={index}>
               {/* {blog.researchTopics.title} */}
              <ResearchCard blog={blog} url={`researchandinsights/${slug}`} />
            </div>
          ))}
      </section>    
      <Footer />
    </div>
  );
}
