'use client'
import Image from "next/image";
import { urlForImage } from '../../../lib/image';
import { client } from '../../../lib/client';
import { PortableText } from "@portabletext/react";
import { useEffect, useState } from "react";
import myPortableTextComponents from "@/assets/PortableTextComponent";
import Marquee from '@/components/Marquee';
import Link from "next/link";
import FileViewer from '@/assets/FileViewer';
import Navbar from '@/components/Navbar';
import Footer from "@/components/Footer";
import { useReadingProgress } from "@/assets/ReadingProgress";
import { useRouter } from "next/navigation";

export default function Page({ params: { slug } }) {
  const [insight, setInsight] = useState(null);
  const [insights, setInsights] = useState([]);
  const completion = useReadingProgress();
  const router = useRouter();

  const fetchInsight = async () => {
    try{
    const insightQuery = `*[_type == 'insight' && slug.current == $slug][0] {
      title, 
      body[]{
        ...,
        "imageUrl": asset->url,
        "imageCaption": caption,
        "fileUrl": asset->url
      }, 
      tags,
      sideLinks,
      "authors": authors[]->{
        name,
        "slug": slug.current
      },
      publishDate,
      image{..., "imageUrl": asset->url}, 
      summary,
      "documentURL": document.asset->url
    }`;
    // const data = await client.fetch(query, { slug });
    // setInsight(data);
 
    const insightsQuery = `*[_type=='insight'] | order(_createdAt asc) {
      title,
      image,
      summary,
      publishDate,
      tagType,
      "slug": slug.current,
      body
    }`;
    // const insights = await client.fetch(query);
    // setInsights(insights);

    const [data,insights] = await Promise.all([
      client.fetch(insightQuery,{slug}),
      client.fetch(insightsQuery)
    ])

    setInsight(data);
    setInsights(insights);

  }catch(error){
    console.log(error)
  }
}

  useEffect(() => {
    fetchInsight();
    // fetchInsights();
  }, [slug]);

  // Listen to real-time updates
  useEffect(() => {
    const subscription = client
      .listen(`*[_type == "insight" && slug.current == $slug]`, { slug })
      .subscribe((update) => {
        fetchInsight(); // Fetch the latest data on update
      });

    return () => subscription.unsubscribe(); // Cleanup on unmount
  }, [slug]);

  if (!insight) {
    return (
      <div className="flex justify-center items-center  min-h-screen">
          <div className="loader"></div>
      </div>
    );
  }

  return (
    <article className="mt-16 px-2 2xl:px-12 flex flex-col">
      <Navbar />
      <nav className="sticky z-50 top-[4.5rem] lg:top-[5.5rem] md:top-[6rem]">
      <span
        id="progress-bar"
        style={{
          transform: `translateX(${completion - 100}%)`,
        }}
        className={`absolute bottom-0 w-full transition-transform duration-150 h-1 bg-white`}
      />
      {/* Rest of the NavBar */}
    </nav>
      {/* Featured Image */}
      <div className="relative w-full h-[500px]">
        <Image
          src={urlForImage(insight.image.imageUrl)}
          layout="fill"
          objectFit="cover"
          alt={insight.title}
          className="w-full h-full"
        />
      </div>

      <div className="flex flex-col md:flex-row mt-8 gap-x-8">
        {/* Side Panel */}
        <div className="flex flex-col items-start mb-5 md:w-[20%] p-12 z-20 bg-white mt-[-130px]">
          <div className="">
            <hr className="my-4 border-t-2 border-gray-300" />
            {insight.publishDate && (
              <div className="text-lg text-gray-600 mb-4">
                {new Date(insight.publishDate).toLocaleDateString('en-US', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric',
                })}
              </div>
            )}
          </div>

          <div className="mb-4 w">
            <hr className="my-4 border-t-2 border-gray-300" />
            <h4 className="text-xl font-semibold mb-2">People</h4>
            {insight.authors && insight.authors.map((author, index) => (
              <div key={index} className="flex items-center gap-x-2 mt-2">
                <Link href={`/our-people/${author.slug}`}>
                  <span className="text-lg hover:text-blue-600">
                    {author.name}
                  </span>
                </Link>
              </div>
            ))}
          </div>

          <div className="mb-4 ">
            <hr className="my-4 border-t-2 border-gray-300" />
            <h4 className="text-xl font-semibold mb-2">Topics</h4>
            <div className="flex flex-col gap-2">
              {insight.tags && insight.tags.map((tag, index) => (
                <div key={index} className="text-blue-800 px-2 py-1 rounded">
                  <button
                  type="button"
                  onClick={() => {
                    router.push(`/search?query=${tag}`);
                  }}
                >
                  #{tag}
                </button>
                </div>
              ))}
            </div>
          </div>

          <div className="">
            <hr className="my-4 border-t-2 border-gray-300" />
            <h4 className="text-xl font-semibold mb-2">In this Group</h4>
            <div className="flex flex-col gap-2">
              {insight.sideLinks && insight.sideLinks.map((link, index) => (
                <a key={index} href={link.url} className="text-blue-600 hover:text-blue-800">
                  {link.title}
                </a>
              ))}
            </div>
          </div>


        </div>

        {/* Insight Title and Body */}
        <div className="md:w-2/3 p-6">
          <div className="text-[2rem] font-normal merriweather   ">
            {insight.title}
          </div>

          <section className="mt-8">
            <div className="text-[1.1rem] leading-snug text-dark/80 ">
              <PortableText value={insight.body} components={myPortableTextComponents} />
            </div>
          </section>
        </div>
      </div>

      {/* Document Viewer */}
      <div className="flex justify-center mt-8">
        {insight.documentURL && (
          <div className="w-full h-96 p-6 md:w-2/3">
            <FileViewer url={insight.documentURL} />
          </div>
        )}
      </div>

      {/* Marquee Section */}
      <div className="flex justify-center">
        <Marquee blogs={insights} url={'our-insights'} />
      </div>

      <Footer />
    </article>
  );
}
