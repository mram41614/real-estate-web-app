'use client'

import Navbar from "../../components/Navbar";
import { client } from '../../lib/client';
import { useEffect, useState } from "react";
import OurInsightCard from "@/components/OurInsight";
import Footer from "@/components/Footer";

export default function Page() {

  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='insight'] | order(_createdAt asc) {
          title,
          image,
          tagType,
          publishDate,
          summary,
          "slug": slug.current,
          body
        }`;

        const posts = await client.fetch(query);
        setBlogs(posts);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  console.log('Blogs : ',blogs)

  return (
    <div className="flex flex-col min-h-screen" >
      <Navbar />
      <div className="flex-grow">
      <div>
        <h1 className="text-[1.5rem] font-bold text-center merriweather mt-28 mb-8 text-gray-500">Our Insights</h1>
      </div>
      <div>
        <section className="mx-6 p-4">
          {
            blogs.map((blog,index)=>(
              <OurInsightCard blog={blog} key={index} />
            ))
          }

        </section>
      </div>
      </div>

      <Footer />
    </div>
  );
}
