
'use client'
import Image from "next/image";
import Navbar from "@/components/Navbar";
import { client } from '../../../lib/client'
import { useEffect, useState } from "react";
import Footer from "@/components/Footer";
import { FaXTwitter } from "react-icons/fa6";
import { FaFacebookF } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa6";
import GuidelineCard from "@/components/GuidelineCard";
import Link from "next/link";

export default function Common() {
  const [blogs, setBlogs] = useState([]);
  const [sidePanel, setSidePanel] = useState(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [guidelinesOpen, setGuidelinesOpen] = useState(false); // New state for guidelines toggle


  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='hotel'] | order(_createdAt asc) {
          _id,
          title,
          "image": image.asset->url,
          "guidelinesHotel": guidelinesHotel[]->{
            title,
            "slug": slug.current,
            summary,
            "authors": authors[]->{
              name,
              "slug": slug.current
            },
            tags
          },
          "slug": slug.current,
          order,
          summary
        }`;

        const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
          sidePanelLinks,
          socialMediaLinks,
        }`;

        const [posts, sidePanelData] = await Promise.all([
          client.fetch(query),
          client.fetch(sidePanelQuery),
        ]);

        setBlogs(posts);
        setSidePanel(sidePanelData[0]);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const toggleGuidelines = () => {
    setGuidelinesOpen(!guidelinesOpen);
  };

  const guidelineLinks = [
    { title: "Common", href: "/guide/common" },
    { title: "Retail", href: "/guide/retail" },
    { title: "Office", href: "/guide/office" },
    { title: "F&B", href: "/guide/fnb" },
    { title: "Hotel", href: "/guide/hotel" }
  ];

  return (
    <div className="flex flex-col min-h-screen bg-white text-gray-900" style={{ backgroundImage: `url('/')`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <Navbar />
      <div className="relative w-full h-[500px]">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
      </div>

      <div className="flex flex-col md:flex-row">
        <div className="flex flex-col mb-5 md:w-[30%] p-4 md:p-12 z-20 bg-white mt-[-90px]">
          <div className="flex flex-col items-center">
            <div className="flex flex-col text-md items-start min-[320px]:w-full md:w-auto gap-5 p-5">
              {/* Sidebar links for larger devices */}
              <div className="hidden md:flex flex-col gap-2 w-full">
                {sidePanel?.sidePanelLinks?.map((link, index) => (
                  <a key={index} href={link.url} className="font-sans text-black hover:text-[#04ADEF] font-bold">
                    {link.title}
                  </a>
                ))}
                <div>
                  <button onClick={toggleGuidelines} className="font-sans text-black hover:text-[#04ADEF] font-bold">
                    Guidelines
                  </button>
                  {guidelinesOpen && (
                    <div className="ml-4 mt-2 flex flex-col gap-2">
                      {guidelineLinks.map((link, index) => (
                        <a key={index} href={`${link.href}`} className="font-sans text-black hover:text-[#04ADEF] font-bold">
                          {link.title}
                        </a>
                      ))}
                    </div>
                  )}
                </div>
              </div>

              {/* Sidebar links for smaller devices */}
              <div className="flex md:hidden w-full flex-col gap-2">
                <div className="flex justify-between w-full ">
                  {sidePanel?.sidePanelLinks?.slice(0, 1).map((link, index) => (
                    <a key={index} href={link.url} className="font-sans text-black hover:text-[#04ADEF] text-center font-bold">
                      {link.title}
                    </a>
                  ))}
                  <div className="relative inline-block text-left">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex justify-center w-auto rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                      More
                    </button>
                    {dropdownOpen && (
                      <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <div className="py-1">
                          {sidePanel?.sidePanelLinks?.slice(1).map((link, index) => (
                            <a key={index} href={link.url} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                              {link.title}
                            </a>
                          ))}
                          <div>
                            <button onClick={toggleGuidelines} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                              Guidelines
                            </button>
                            {guidelinesOpen && (
                              <div className="ml-4 mt-2 flex flex-col gap-2">
                                {guidelineLinks.map((link, index) => (
                                  <a key={index} href={`${link.href}`} className="font-sans text-black hover:text-[#04ADEF] font-bold">
                                    {link.title}
                                  </a>
                                ))}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>

              <hr className="my-4 border-t-2 w-full border-gray-300" />
              <div className="flex flex-row min-[320px]:w-full justify-center md:w-auto gap-4">
                {sidePanel?.socialMediaLinks?.map((link, index) => {
                  let icon;
                  switch (link.title) {
                    case 'X':
                      icon = <FaXTwitter size={18} />;
                      break;
                    case 'Facebook':
                      icon = <FaFacebookF size={18} />;
                      break;
                    case 'Instagram':
                      icon = <FaInstagram size={18} />;
                      break;
                    case 'LinkedIn':
                      icon = <FaLinkedinIn size={18} />;
                      break;
                    default:
                      icon = link.title; // Fallback to the title if no match is found
                  }
                  return (
                    <a key={index} href={link.url} className="text-black hover:text-[#04ADEF]">
                      {icon}
                    </a>
                  );
                })}
              </div>
            </div>
          </div>
        </div>

        {/* Blog Cards */}
        <div className="md:w-full bg-white grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3">
          {blogs.map((blog, index) => (
            <div key={index} className="hover:text-blue-500">
              <Link href={`/guide/hotel/${blog.slug}`} >
                <GuidelineCard blog={blog} url={'/guide/hotel'} />
              </Link>
            </div>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}
