// import React from 'react'
// import Image from 'next/image';
// import NavBar from '@/components/Navbar';


// const page = ({params:{slug}}) => {
 
//     useEffect(() => {
//         const fetchData = async () => {
//           try {
//             const query = `*[_type=='common' && slug.current==$slug] | order(_createdAt asc) {
//               _id,
//               title,
//               "image": image.asset->url,
//               "guidelinesCommon": guidelinesCommon[]->{
//                 title,
//                 "slug": slug.current,
//                 summary,
//                 "authors": authors[]->{
//                   name,
//                   "slug": slug.current
//                 },
//                 tags
//               },
//               "slug": slug.current,
//               order,
//               summary
//             }`;
    
//             const sidePanelQuery = `*[_type=='sidePanel'] | order(_createdAt asc) {
//               sidePanelLinks,
//               socialMediaLinks,
//             }`;
    
//             const [posts, sidePanelData] = await Promise.all([
//               client.fetch(query),
//               client.fetch(sidePanelQuery),
//             ]);
    
//             setBlogs(posts);
//             setSidePanel(sidePanelData[0]);
//           } catch (error) {
//             console.error("Error fetching data:", error);
//           }
//         };
    
//         fetchData();
//       }, []);

//   return (
//     <>
//     This is {slug}
//     {/* <NavBar />
//     <div className="flex-grow  mt-28  ">
//         <section className="grid mt-20 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4 ">
//           {blogs.map((blog, index) => (
//             <div className='group border h-96  p-9 '>
//              <div>
//                 <Image src={blog.image} className='ml-[-7px]' width={200} height={200} />
//              </div>
//             <div className="group-hover:text-[#04ADEF] text-lg leading-snug font-sans transition duration-300 mt-5 font-light hover:text-blue-700 ">
//             {blog.title}
//           </div>
//           <div className=" mt-5 font-light hover:text-[#04ADEF] ">
//             {blog.author}
//           </div>
//           <div className="flex flex-row font-light hover:text-[#04ADEF] ">
//             {blog.tags.map((tag,index)=>(
//                 <div className="  font-light hover:text-[#04ADEF] ">
//                 #{tag}
//               </div>
//             ))}
//           </div>
           
//           </div>
//           ))}
//         </section>
//       </div> */}
//       </>
//   )
// }

// export default page

"use client"
import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import NavBar from '@/components/Navbar';
import {client} from '../../../../lib/client';
import HighlightCurrentLink from '@/components/HighlightCurrentLink';

const Page = ({ params: { slug } }) => {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const query = `*[_type=='common' && slug.current==$slug] {
          _id,
          title,
          "image": image.asset->url,
          "guidelinesCommon": guidelinesCommon[]->{
            title,
            "image": image.asset->url,
            "slug": slug.current,
            summary,
            "authors": authors[]->{
              name,
              "slug": slug.current
            },
            tags
          },
          "slug": slug.current,
          order,
          summary
        }`;

        const [posts] = await Promise.all([
          client.fetch(query, { slug }),
        ]);

        setBlogs(posts[0].guidelinesCommon);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [slug]);



  

  return (
    <>
      <NavBar />
      <div className="relative w-full h-[500px]">
        <Image
          src='/mitbg.jpg'
          layout="fill"
          objectFit="cover"
          alt='mitbg'
          className="w-full h-full"
        />
      </div>
        <HighlightCurrentLink />
      <div className="flex-grow">
        
        <section className="grid mt-2 grid-cols-1 text-[#ACACAC] sm:grid-cols-2 lg:grid-cols-4">
          {blogs.map((blog, index) => (
            <Link href={`/guide/common/${slug}/${blog.slug}`} >
            <div key={index} className='group border h-96 p-9'>
            <div className="w-full h-[150px] relative">
                  <Image src={blog.image} alt={blog.title} layout="fill" objectFit="cover" />
                </div>
              <div className="group-hover:text-[#04ADEF] text-lg leading-snug font-sans transition duration-300 mt-5 font-light hover:text-blue-700">
                {blog.title}
              </div>
              <div className="mt-5 font-light hover:text-[#04ADEF]">
                {blog.authors.map((author, index) => (
                  <span key={index}>{author.name}</span>
                ))}
              </div>
              <div className="flex flex-wrap font-light hover:text-[#04ADEF]">
                {blog?.tags?.map((tag, index) => (
                  <div key={index} className="font-light hover:text-[#04ADEF]">
                    #{tag}
                  </div>
                ))}
              </div>
            </div>
            </Link>
          ))}
        </section>
      </div>
    </>
  );
};

export default Page;
