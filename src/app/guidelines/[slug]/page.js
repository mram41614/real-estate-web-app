'use client'
import Image from "next/image";
import { client } from '../../../lib/client';
import { useEffect, useState } from "react";
import Link from "next/link";
import Navbar from '@/components/Navbar';
import Footer from "@/components/Footer";
import LuxuryCard from "@/components/LuxuryCard";

export default function Page({ params: { slug } }) {
  const [commonCards, setCommonCards] = useState([]);
  const [uniqueCards, setUniqueCards] = useState([]);
  const [clickedCardId, setClickedCardId] = useState(null);

  const fetchInsight = async () => {
    try {
      const commonCardsQuery = `*[_type == 'common'] {
        _id,
        title,
        "image": image.asset->url,
        "guidelinesCommon": guidelinesCommon[]->{
          title,
          "slug": slug.current
        },
        order,
        summary
      }`;

      const uniqueCardsQuery = `*[_type == "${slug}" && "${slug}" != 'common'] {
        _id,
        title,
        "image": image.asset->url,
        order,
        summary,
        "guidelines": guidelines${slug.charAt(0).toUpperCase() + slug.slice(1)}[]->{
          title,
          "slug": slug.current
        }
      }`;

      const [commonData, uniqueData] = await Promise.all([
        client.fetch(commonCardsQuery),
        client.fetch(uniqueCardsQuery, { slug: slug.charAt(0).toUpperCase() + slug.slice(1) })
      ]);

      const sortedCommonData = commonData.sort((a,b)=> a.order - b.order);
      const sortedUniqueData = uniqueData.sort((a,b)=> a.order - b.order);

      setCommonCards(sortedCommonData);
      setUniqueCards(sortedUniqueData);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    fetchInsight();
  }, [slug]);

  const handleCardClick = (id) => {
    setClickedCardId(clickedCardId === id ? null : id);
  }

  if (!commonCards.length && !uniqueCards.length) {
    return (
      <div className="flex justify-center items-center min-h-screen">
        <div className="loader"></div>
      </div>
    );
  }

  console.log('Common Cards:', commonCards);
  console.log('Unique Cards:', uniqueCards);

  return (
    <>  
      <Navbar />
      <div>
        <h1 className="md:text-7xl text-xl mt-20 p-5 lg:text-9xl text-black relative z-20 bg-clip-text text-transparent bg-gradient-to-b from-neutral-200 to-neutral-600 text-center font-bold">
          Common
        </h1> 
        <div className="grid grid-cols-1 mt-2 p-5 md:grid-cols-2 lg:grid-cols-4 gap-4 w-full">  
          {commonCards.filter(data => data.title).map((data) => (
            <div
              key={data._id}
              className={`flex flex-col cursor-pointer ${clickedCardId === data._id ? 'bg-white' : ''}`}
              onClick={() => handleCardClick(data._id)}
            >
              <LuxuryCard data={data} guidelines={data.guidelinesCommon} isActive={clickedCardId === data._id} slug="common" />
            </div>
          ))}
        </div>
        {uniqueCards.length > 0 && (
          <div>
            <h1 className="md:text-7xl text-xl mt-2 p-5 lg:text-9xl text-black relative z-20 bg-clip-text text-transparent bg-gradient-to-b from-neutral-200 to-neutral-600 text-center font-bold">
              {slug === 'fnb' ? 'F & B' : slug.charAt(0).toUpperCase() + slug.slice(1)}
            </h1> 
            <div className="grid grid-cols-1 mt-2 p-5 md:grid-cols-2 lg:grid-cols-4 gap-4 w-full"> 
              {uniqueCards.filter(data => data.title).map((data) => (
                <div
                  key={data._id}
                  className={`flex flex-col cursor-pointer ${clickedCardId === data._id ? 'bg-white' : ''}`}
                  onClick={() => handleCardClick(data._id)}
                >
                  <LuxuryCard data={data} guidelines={data.guidelines} isActive={clickedCardId === data._id} slug={slug} />
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
      <Footer />
    </>
  );
}
