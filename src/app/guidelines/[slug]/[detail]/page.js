


'use client'

import Image from 'next/image';
import { urlForImage } from '../../../../lib/image';
import { client } from '../../../../lib/client';
import { PortableText } from "@portabletext/react";
import { useEffect, useState } from 'react';
import myPortableTextComponents from "@/assets/PortableTextComponent";
import Marquee from '@/components/Marquee';
import Link from 'next/link';
import FileViewer from '@/assets/FileViewer';
import Navbar from '@/components/Navbar';
import Footer from '@/components/Footer';
import { useReadingProgress } from '@/assets/ReadingProgress';
import { useRouter } from 'next/navigation';

export default function Page({ params: { detail } }) {
  const [guideline, setGuideline] = useState(null);
  const [guidelines, setGuidelines] = useState([]);
  const completion = useReadingProgress();
  const router = useRouter();

  const fetchGuideline = async () => {
    try {
      const guidelineQuery = `*[_type in ["guidelinesCommon", "guidelinesRetail", "guidelinesOffice", "guidelinesFnb", "guidelinesHotel"] && slug.current == $detail ][0] {
        title, 
        body[] {
          ...,
          "imageUrl": asset->url,
          "imageCaption": caption,
          "fileUrl": asset->url,
          _type == 'mux.video' => {
            "playbackId": asset->playbackId
          }
        }, 
        video {
          asset-> {
            "playbackId": playbackId
          }
        },
        tags,
        sideLinks,
        "authors": authors[]-> {
          name,
          "slug": slug.current
        },
        publishDate,
        image { ..., "imageUrl": asset->url }, 
        summary,
        "documentURL": document.asset->url
      }`;

      const guidelinesQuery = `*[_type in ["guidelinesCommon", "guidelinesRetail", "guidelinesOffice", "guidelinesFnb", "guidelinesHotel"]] | order(_createdAt asc) {
        title,
        image,
        summary,
        publishDate,
        "slug": slug.current,
        body
      }`;

      const [data, guidelines] = await Promise.all([
        client.fetch(guidelineQuery, { detail }),
        client.fetch(guidelinesQuery)
      ]);

      setGuideline(data);
      setGuidelines(guidelines);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchGuideline();
  }, [detail]);

  useEffect(() => {
    const subscription = client
      .listen(`*[_type in ["guidelinesCommon", "guidelinesRetail", "guidelinesOffice", "guidelinesFnb", "guidelinesHotel"] && slug.current == $detail]`, { detail })
      .subscribe(() => {
        fetchGuideline();
      });

    return () => subscription.unsubscribe();
  }, [detail]);

  if (!guideline) {
    return (
      <div className="flex justify-center items-center min-h-screen">
        <div className="loader"></div>
      </div>
    );
  }

  return (
    <article className="mt-16 px-2 2xl:px-12 flex flex-col">
      <Navbar />
      <nav className="sticky z-50 top-[4.5rem] lg:top-[5.5rem] md:top-[6rem]">
        <span
          id="progress-bar"
          style={{
            transform: `translateX(${completion - 100}%)`,
          }}
          className={`absolute bottom-0 w-full transition-transform duration-150 h-1 bg-yellow-400`}
        />
      </nav>

      {/* Featured Image */}
      {guideline.image?.imageUrl && (
        <div className="relative w-full h-[500px]">
          <Image
            src={urlForImage(guideline.image.imageUrl)}
            layout="fill"
            objectFit="cover"
            alt={guideline.title}
            className="w-full h-full"
          />
        </div>
      )}

      <div className="flex flex-col md:flex-row mt-8 gap-x-8">
        {/* Side Panel */}
        <div className="flex flex-col items-start mb-5 md:w-[20%] p-12 z-20 bg-white mt-[-130px]">
          {guideline.publishDate && (
            <div>
              <hr className="my-4 border-t-2 border-gray-300" />
              <div className="text-lg text-gray-600 mb-4">
                {new Date(guideline.publishDate).toLocaleDateString('en-US', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric',
                })}
              </div>
            </div>
          )}

          {guideline.authors?.length > 0 && (
            <div className="mb-4 w">
              <hr className="my-4 border-t-2 border-gray-300" />
              <h4 className="text-xl font-semibold mb-2">People</h4>
              {guideline.authors.map((author, index) => (
                <div key={index} className="flex items-center gap-x-2 mt-2">
                  <Link href={`/our-people/${author.slug}`}>
                    <span className="text-lg hover:text-blue-600">
                      {author.name}
                    </span>
                  </Link>
                </div>
              ))}
            </div>
          )}

          {guideline.tags?.length > 0 && (
            <div className="mb-4 ">
              <hr className="my-4 border-t-2 border-gray-300" />
              <h4 className="text-xl font-semibold mb-2">Topics</h4>
              <div className="flex flex-col gap-2">
                {guideline.tags.map((tag, index) => (
                  <div key={index} className="text-blue-800 px-2 py-1 rounded">
                    <button
                      type="button"
                      onClick={() => {
                        router.push(`/search?query=${tag}`);
                      }}
                    >
                      #{tag}
                    </button>
                  </div>
                ))}
              </div>
            </div>
          )}

          {guideline.sideLinks?.length > 0 && (
            <div>
              <hr className="my-4 border-t-2 border-gray-300" />
              <h4 className="text-xl font-semibold mb-2">In this Group</h4>
              <div className="flex flex-col gap-2">
                {guideline.sideLinks.map((link, index) => (
                  <a key={index} href={link.url} className="text-blue-600 hover:text-blue-800">
                    {link.title}
                  </a>
                ))}
              </div>
            </div>
          )}
        </div>

        {/* Guideline Title and Body */}
        <div className="md:w-2/3 p-6">
          <h1 className="text-5xl font-serif lg:text-6xl font-bold text-dark ">
            {guideline.title}
          </h1>

          <section className="mt-8">
            <div className="text-lg leading-normal text-dark/80 ">
              <PortableText value={guideline.body} components={myPortableTextComponents} />
            </div>
          </section>
        </div>
      </div>

      {/* Document Viewer */}
      {guideline.documentURL && (
        <div className="flex justify-center mt-8">
          <div className="w-full h-96 p-6 md:w-2/3">
            <FileViewer url={guideline.documentURL} />
          </div>
        </div>
      )}

      {/* Marquee Section */}
      {/* <div className="flex justify-center">
        <Marquee blogs={guidelines} url={'our-insights'} />
      </div> */}

      <Footer />
    </article>
  );
}

