'use client'
import Image from "next/image";
import { client } from "@/lib/client";
import { urlForImage } from "@/lib/image";
import { PortableText } from "@portabletext/react";
import { useEffect, useState} from "react";
import myPortableTextComponents from "@/assets/PortableTextComponent";
import Marquee from '@/components/Marquee';
import Link from "next/link";
import FileViewer from '@/assets/FileViewer';
import Navbar from '@/components/Navbar';
import Footer from "@/components/Footer";
import { useRouter } from 'next/navigation';
import SidePanel from "@/components/SidePanel";


export default function Page() {
  const [blog, setBlog] = useState(null);
  const [blogs, setBlogs] = useState([]);
  const router = useRouter();


  const fetchBlog = async () => {

    try{
    const postQuery = `*[_type == 'about'][0] {
      title, 
      body[]{
        ...,
        "imageUrl": asset->url,
        "imageCaption": caption,
        "fileUrl": asset->url,
        _type == 'mux.video' => {
          "playbackId": asset->playbackId
        },
        _type == 'Table' => {
          rows
        }
      }, 
      video{
        asset->{
          "playbackId": playbackId
        }
      },
      tags,
      
      "authors": authors[]->{
        name,
        "slug": slug.current
      },     
      image{..., "imageUrl": asset->url}, 
      summary,
      "documentURL": document.asset->url
    }`;


  
    const postsQuery = `*[_type=='post'] | order(_createdAt asc) {
      title,
      image,
      summary,
      publishDate,
      tagType,
      "slug": slug.current,
      body
    }`;
    // const posts = await client.fetch(query);
    // setBlogs(posts);

      const [data,posts] = await Promise.all([
              client.fetch(postQuery),
              client.fetch(postsQuery)
      ]);

      setBlog(data);
      setBlogs(posts);
    }catch(error){
      console.log(error)
    }
  }

  useEffect(() => {
    fetchBlog();
    // fetchBlogs();
  }, []);

  // Listen to real-time updates
  useEffect(() => {
    const subscription = client
      .listen(`*[_type == "about"]`)
      .subscribe((update) => {
        fetchBlog(); // Fetch the latest data on update
      });

    return () => subscription.unsubscribe(); // Cleanup on unmount
  }, []);

  if (!blog) {
    return (
      <div className="flex justify-center items-center  min-h-screen">
          <div className="loader"></div>
      </div>
    );
  }
  // console.log(blog.body[0].imageCaption)

  return (
    
    <article className="   flex flex-col">
      <Navbar />
      {/* Featured Image */}
      <div className="relative w-full h-[500px]">
        <Image
          src={urlForImage(blog?.image?.imageUrl)}
          layout="fill"
          objectFit="cover"
          alt={blog.title}
          className="w-full h-full"
        />
      </div>
      
      <div className="flex flex-col md:flex-row mt-8 gap-x-8">
        <SidePanel />

        {/* Blog Title and Body */}
        <div className="md:w-2/3 p-6">
        <h1 className="text-[2rem] leading-snug  line">
            {blog.title}
          </h1>

          <section className="mt-8">
            <div className="text-lg leading-normal text-dark/80 ">
              <PortableText className="leading-10" value={blog.body} components={myPortableTextComponents} />
            </div>
          </section>
        </div>
      </div>

      {/* Document Viewer */}
      <div className="flex justify-center mt-8">
        {blog.documentURL && (
          <div className="w-full h-96 p-6 md:w-2/3">
            <FileViewer url={blog.documentURL} />
          </div>
        )}
      </div>

      {/* Marquee Section */}
      <div className="flex justify-center">
        <Marquee blogs={blogs} url={'how-we-help-clients'} />
      </div>

      <Footer />
    </article>
  );
}
